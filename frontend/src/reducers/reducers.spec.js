import { analysisData } from './index';

describe('analysisData reducer', () => {
    it('should return the initial state', () => {
        expect(analysisData(undefined, {})).toEqual({});
    });
  
    it('should set given corpus files', () => {
        const exampleFiles = [
            {
                fileanme: 'test'
            }
        ];
        expect(analysisData({}, {type: 'SET_ANALISYS_RESULT', data: exampleFiles }))
            .toEqual(exampleFiles);
    });

    it('should add selected corpus files', () => {
        const beforeState = {
            corpus_part_files: {
                'ab':  {
                    categories: {
                        kind: 'sport',
                        year:  1988
                    }
                },
                'cd': {
                    categories: {
                        kind: 'sport'
                    }
                }
            }
        };
        const categoriesFilter = {year:[{value:1988,label:1988}]};
        const expectedOutputState = {
            ...beforeState,
            selected: {
                'ab': true,
            }
        };
        expect(
            analysisData(
                beforeState, 
                {
                    type: 'FILTER_SELECTED_CORPUS_FILES',
                    categoriesFilter: categoriesFilter
                })
        ).toEqual(expectedOutputState);
    });

    it('should deselect selected', () => {
        const beforeState = {
            corpus_part_files: {
                'ab':  {
                    categories: {
                        kind: 'sport',
                        year:  1988
                    }
                },
                'cd': {
                    categories: {
                        kind: 'sport'
                    }
                }
            },
            selected: {'ab': true, 'cd':true}
        };
        const categoriesFilter = {};
        const expectedOutputState = {
            ...beforeState,
            selected: {},
        };
        expect(
            analysisData(
                beforeState, 
                {
                    type: 'FILTER_SELECTED_CORPUS_FILES', 
                    categoriesFilter: categoriesFilter
                })
        ).toEqual(expectedOutputState);
    });

    it('should handle different data types', () => {
        const beforeState = {
            corpus_part_files: {
                'ab':  {
                    categories: {
                        kind: 'sport',
                        year:  1988
                    }
                }
            }
        };
        const categoriesFilter = {year:[{value:'1988',label: '1988'}]};
        const expectedOutputState = {
            ...beforeState,
            selected: {'ab': true}
        };
        expect(
            analysisData(
                beforeState, 
                {
                    type: 'FILTER_SELECTED_CORPUS_FILES', 
                    categoriesFilter: categoriesFilter
                })
        ).toEqual(expectedOutputState);
    });

    it('should return intersection', () => {
        const beforeState = {
            corpus_part_files: {
                'ab': {
                    categories: {
                        year:  1988,
                        kind: 'event'
                    }
                },
                'bc': {
                    categories: {
                        year:  1988,
                        kind: 'sport'
                    }
                }
            }
        };
        const categoriesFilter = {
            year: [{value:'1988',label: '1988'}],
            kind: [{value:'sport',label: 'sport'}]
        };
        const expectedOutputState = {
            ...beforeState,
            selected: {'bc': true}
        };
        expect(
            analysisData(
                beforeState,
                {
                    type: 'FILTER_SELECTED_CORPUS_FILES',
                    categoriesFilter: categoriesFilter
                })
        ).toEqual(expectedOutputState);
    });
});
