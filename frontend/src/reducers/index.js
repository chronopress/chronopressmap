import { combineReducers } from 'redux';
import { ResultState } from '../result-state';

export const categories = (state = {}, action) => {
    if (action.type === 'SET_CATEGORIES'){
        return action.categories;
    } else {
        return state;
    }
};


export const analysisData = (state = {}, action) => {
    switch(action.type){
    case 'SET_ANALISYS_RESULT':
        return action.data;
    case 'FILTER_SELECTED_CORPUS_FILES':
        if (!state.corpus_part_files){ 
            return state;
        }
        const selected = getSelectedFilesIds(state.corpus_part_files, action.categoriesFilter);
        return {
            ...state,
            selected
        };
    default:
        return state;
    }
};


const getSelectedFilesIds = (files, categoriesFilter) => {
    return  Object.keys(files).reduce((selectedFiles, fileId) => {
        const selected = Boolean(Object.keys(categoriesFilter).length)
            && Object.keys(categoriesFilter).every((category) => {
                const categoryValues = categoriesFilter[category].map(item => item.value);
                const fileCategory = files[fileId].categories[category];
                // eslint-disable-next-line
                return fileCategory ? categoryValues.some(el => el == fileCategory) : false;
            });
        if(selected){
            selectedFiles[fileId] = true;
        }
        return selectedFiles;
    }, {});
};

const lang = (state = 'en', action) => {
    switch(action.type){
    case 'SET_LANG':
        return action.lang;;
    default:
        return state;
    }
};

const editMode = (state = false, action) => {
    switch(action.type){
    case 'TOGGLE_EDIT_MODE':
        return !state;
    default:
        return state;
    }
};

const changedLocations = (state={}, action) => {
    switch(action.type){
    case 'ADD_CHANGED_LOCATION':
        return {
            ...state,
            [action.changedLocation.locationId]: action.changedLocation
        };
    case 'CLEAR_CHANGED_LOCATION':
        return {};
    default:
        return state;
    }
};

const resultState = (state = ResultState.NoResult, action) =>{
    switch(action.type){
    case 'RESULT_FETCHED':
        return ResultState.ResultReady;
    case 'RESULT_UPDATE':
        return ResultState.UpdateResult;
    default:
        return state;
    }
};

export const rootReducer = combineReducers({
    categories,
    analysisData,
    lang,
    editMode,
    changedLocations,
    resultState,
});
