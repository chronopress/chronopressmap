import React, { Component } from 'react';
import { connect } from 'react-redux';

import L from 'leaflet';
import { GeoJSON, Map, Popup, TileLayer } from 'react-leaflet';
import { Sidebar, Tab } from 'react-leaflet-sidebarv2';

import counterpart from 'counterpart';
import Translate from 'react-translate-component';

import CategoryFilter from './CategoryFilter';
import EditPointView  from './EditPointView.jsx';

import { getPreferredIconAndColourForCount } from './icons/Icons';
import { splitAggregationData, zoomToAggregationMapping, ZoomThreshold } from '../utils/aggregator';
import { bindArgsFromN } from '../utils/helpers';
import { getSelectedLocationsData } from '../utils/locationsFilter';


class MapView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mapCenter: [51.1078852, 17.0385376],
            zoom: 8,
            collapsed: false,
            selected: 'home',
            averageCount: 0,
            selectedAggregations: {},
            selectedAggregatedLocations: [],
            selectedNonAggregatedLocations: [],
            locationsVisibility: {},
            map: null
        };
        this.createCustomMarker = this.createCustomMarker.bind(this);
    }

    onClose() {
        this.setState({ collapsed: true });
    }

    onOpen(id) {
        this.setState({
            collapsed: false,
            selected: id
        });
    }

    createCustomMarker(feature, latlng, icon, location) {
        const draggable = () => this.props.editMode && this.state.zoom >= ZoomThreshold.FIRST_AGGREGATION; 
        const marker = L.marker(latlng, {icon, draggable});
        marker.on('dragend', (e) => {
            this.props.addChangedLocation(location._id, location.name, e.target.getLatLng());
        });
        return marker;
    }

    createLocationPopup(location) {
        return ('mostCommonLocation' in location) ?
            <Popup>
                <div>
                    <p><Translate content="map.aggregationName"/>: {location.aggregatedLocationsCount}, <Translate content="map.totalOccurrences"/>: {location.count}.</p>
                    <p><Translate content="map.mostHits"/>: {location.mostCommonLocation}, {location.mostOccurrences}</p>
                </div>
            </Popup>
            :
            <Popup>
                <div>
                    <p>{location.name},  <Translate content="map.occurrences"/>: {location.count}</p>
                </div>
            </Popup>;
    }

    createGeoJSON(location) {
        const defaultPopup = this.createLocationPopup(location);
        const [icon, colour] = getPreferredIconAndColourForCount(location.count, this.state.averageCount);
        const locationID = location._id;

        return <GeoJSON
            key={locationID}
            style={{fill: false, color: colour}}
            data={location.geometry.geo_json}
            pointToLayer={bindArgsFromN(this.createCustomMarker, 3, icon, location)}
        >
            {defaultPopup}
        </GeoJSON>;
    }

    shouldComponentUpdate(nextProps){
        return !this.props.editMode;
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        counterpart.setLocale(this.props.lang);
        if (JSON.stringify(prevProps.locationsData) !== JSON.stringify(this.props.locationsData)) {
            const aggregatorData = splitAggregationData(this.props.locationsData);
            const aggregatedLocations = aggregatorData.selectedAggregations[zoomToAggregationMapping(this.state.zoom)];

            this.setState(
                {
                    averageCount: aggregatorData.avgCount,
                    selectedAggregations: aggregatorData.selectedAggregations,
                    selectedAggregatedLocations: aggregatedLocations,
                    selectedNonAggregatedLocations: aggregatorData.nonAggregatedLocations,
                    locationsVisibility: aggregatorData.locationsVisibility
                }
            );
        }
    }

    componentDidMount() {
        this.setState({
            map: this.refs.map.leafletElement
        });
    }

    onZoomChange(e) {
        const zoom = e.target._zoom;
        const previousZoom = this.state.zoom;
        const aggregationLevel = zoomToAggregationMapping(zoom);
        const previousAggregationLevel = zoomToAggregationMapping(previousZoom);

        const aggregatedLocations = this.state.selectedAggregations[aggregationLevel];
        if (aggregationLevel !== previousAggregationLevel && aggregatedLocations) {
            this.setState({
                selectedAggregatedLocations: aggregatedLocations
            });
        }
        this.setState({
            zoom: zoom
        });
    }

    checkBoundaries(locations) {
        const bounds = this.state.map.getBounds();
        const southWest = bounds['_southWest'];
        const northEast = bounds['_northEast'];

        let locationsVisibility = this.state.locationsVisibility;
        locations.forEach(location => {
            const locationData = location.props.data;
            if (locationData['type'] === 'Point') {
                locationsVisibility[location.key] = locationData.coordinates[0] <= northEast['lng']
                    && locationData.coordinates[0] >= southWest['lng']
                    && locationData.coordinates[1] <= northEast['lat']
                    && locationData.coordinates[1] >= southWest['lat'];
            }
        });

        this.setState({
            locationsVisibility: locationsVisibility
        });
    }

    render() {
        let selectedLocations = (this.state.selectedAggregatedLocations || []);
        selectedLocations = selectedLocations.concat(this.state.selectedNonAggregatedLocations || []);
        const locations = selectedLocations.map(location => this.createGeoJSON(location));

        return (
            <div>
                <Sidebar
                    id="sidebar"
                    collapsed={this.state.collapsed}
                    selected={this.state.selected}
                    onOpen={this.onOpen.bind(this)}
                    onClose={this.onClose.bind(this)}
                    closeIcon="fa fa-caret-right"
                    position="right"
                >
                    <Tab id="home" header="Filters" icon="fa fa-filter">
                        <CategoryFilter/>
                    </Tab>
                    <Tab id="edit-view" header="Edit Points" icon="fa fa-pen">
                        <EditPointView/>
                    </Tab>
                </Sidebar>
                <Map
                    ref={'map'}
                    preferCanvas={true}
                    onMoveEnd={(e) => this.checkBoundaries(locations)}
                    center={this.state.mapCenter}
                    zoom={this.state.zoom}
                    style={{width: '100%', height: window.innerHeight}}
                    className="sidebar-map"
                    onZoomEnd={(e) => this.onZoomChange(e)}
                >
                    <TileLayer
                        url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    />
                    {locations.filter(location => this.state.locationsVisibility[location.key])}
                </Map>
            </div>
        );
    }
}


const mapStateProps = state => {
    return {
        locationsData: getSelectedLocationsData(state.analysisData),
        lang: state.lang,
        editMode: state.editMode
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addChangedLocation: (locationId, name, newLatLng) => dispatch({type: 'ADD_CHANGED_LOCATION', changedLocation: {locationId, name, newLatLng}})
    };
};

export default connect(mapStateProps, mapDispatchToProps)(MapView);
