import React, { Component } from 'react';
import { connect } from 'react-redux';
import MapView from './MapView';
import { api } from '../api';
import { ResultState } from '../result-state';


class ResultView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            '404': false,
            resultUUID: this.props.match.params.uuid
        };
    }

    async componentDidMount(){
        if (this.props.resultState === ResultState.NoResult){
            await this.fetchResult();
        }
    }

    async componentDidUpdate() {
        if (this.props.resultState === ResultState.UpdateResult){
            const changedLocations = this.props.changedLocations;
            if (Object.keys(changedLocations).length === 0){
                this.props.setResultReady();
            } else {
                const locationsToUpddate = Object.keys(changedLocations).map(locId => {
                    const location = changedLocations[locId];
                    return {
                        uuid: locId,
                        center_lat: location.newLatLng.lat,
                        center_lng: location.newLatLng.lng,
                    };
                });
                try{
                    const resp = await api.editLocations(locationsToUpddate, this.state.resultUUID, this.props.lang);
                    console.log(resp);
                }  catch(err){
                    console.log(err.response);
                }                  
                this.reloadPage();
            }
        }
    }

    reloadPage(){
        window.location.reload(false);
    }

    async fetchResult(){
        try{
            const response = await api.fetchResult(this.state.resultUUID);
            this.setData(response.data);
        }  catch(error) {
            this.setState({'404': true});
        }
    }

    setData(data) {
        this.props.setCategories(data.result.all_categories);
        this.props.setAnalysisData(data);
        this.props.setResultReady();
    }

    render() {
        if (this.state['404']) {
            return (
                <div>Not found</div>
            );
        }

        return (
            <MapView />
        );
    }
}

const mapStateToProps = state => {
    return {
        resultState : state.resultState,
        changedLocations: state.changedLocations,
        lang: state.lang,
    };
};


const mapDispatchToProps = dispatch =>  {
    return {
        setCategories: (categories) => dispatch({type: 'SET_CATEGORIES', categories: categories}),
        setAnalysisData: (data) => dispatch({type: 'SET_ANALISYS_RESULT', data: data}),
        setResultReady: () => dispatch({type: 'RESULT_FETCHED'}),
        clearChangedLocations: () => dispatch({type: 'CLEAR_CHANGED_LOCATION'})
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ResultView);
