import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import counterpart from 'counterpart';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';
import Translate from 'react-translate-component';


const EditPointView = props => {
    useEffect(() => {counterpart.setLocale(props.lang);}, [props]);

    const updateResult = () => {
        props.toggleEditMode();
        props.updateResult();
    };

    const cancelUpdate = () => {
        props.toggleEditMode();
        props.clearChangedLocations();
    };

    const editControls = props.editMode 
        ? <>
            <Button variant="outline-info" onClick={cancelUpdate}>
                <Translate content="editView.cancel"/>
            </Button>
            <Button variant="outline-info" onClick={updateResult} style={{marginLeft:5}}>
                <Translate content="editView.save"/>
            </Button>
        </>
        : <Button variant="outline-info" onClick={() => props.toggleEditMode()}>
            <Translate content="editView.edit"/>
        </Button>; 

    return (
        <div style={{marginTop: 5}}>
            <div style={{marginLeft: '30%'}}>
                {editControls}
            </div>
            {props.editMode && (
                <div style={{marginTop: 20}}>
                    <h4> <Translate content="editView.changedLocations"/></h4>
                    <ListGroup>
                        {Object.keys(props.changedLocations).map(locId => {
                            const location = props.changedLocations[locId];
                            return <ListGroup.Item key={location.name}>
                                <ChangedLocation location={location}/>
                            </ListGroup.Item>;
                        })}
                    </ListGroup>
                </div>
            )}
        </div>
    );
};

const ChangedLocation = props => {
    const {name, newLatLng} = props.location;
    return <>
        {name}: lat:{newLatLng.lat} lng:{newLatLng.lng}
    </>;
};

const mapStateToProps = state => {
    return {
        lang: state.lang,
        editMode: state.editMode,
        changedLocations: state.changedLocations
    };
};

const mapDispatchToProps = dispatch => {
    return {
        toggleEditMode: () => dispatch({type: 'TOGGLE_EDIT_MODE'}),
        updateResult: () => dispatch({type: 'RESULT_UPDATE'}),
        clearChangedLocations: () => dispatch({type: 'CLEAR_CHANGED_LOCATION'})
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPointView);