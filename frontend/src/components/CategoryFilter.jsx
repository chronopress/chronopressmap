import React, { useState } from 'react';
import { connect } from 'react-redux';
import Translate from 'react-translate-component';
import Button from 'react-bootstrap/Button';
import Category from './Category';
import { omitEmptyValues } from '../utils';


const makeSelectOption = value => {
    return {
        value: value,
        label: value
    };
};

const categoriesToSelectOptions = categories => {
    const options = {};
    Object.keys(categories).forEach(key => {
        options[key] = categories[key].map(makeSelectOption);
    });
    return options;
};


const CategoryFilter = (props) =>  {
    const [selected, setSelected] = useState({});
    const options = categoriesToSelectOptions(props.categories);
    const onSelectChange = (category, selectedItems) => {
        setSelected({...selected, [category]: selectedItems});
    };

    return (
        <>
            {Object.keys(props.categories).map(key =>
                <div style={{ marginTop: 15 }} key={key}>
                    <Category
                        title={key}
                        selected={selected[key]}
                        options={options[key]}
                        onChange={(selectedItems) => onSelectChange(key, selectedItems)}
                    />
                </div>
            )}
            <Button
                variant="outline-primary"
                onClick={() => props.filterCorpusFiles(selected)}
                style={{
                    marginTop: 15,
                    marginLeft: '40%'
                }}
                disabled={props.editMode}
            >
                <Translate content="filters.update"/>
            </Button>
        </>
    );
};

const mapDispatchToProps = dispatch => {
    return {
        filterCorpusFiles: (categoriesFilter) => dispatch({
            type: 'FILTER_SELECTED_CORPUS_FILES',
            categoriesFilter: omitEmptyValues(categoriesFilter)
        })
    };
};

const mapStateToProps = state => {
    return {
        categories: state.categories,
        editMode: state.editMode
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryFilter);
