import React, {useEffect} from 'react';
import { Card, Container } from 'react-bootstrap';
import counterpart from 'counterpart';
import FileUpload from './FileUpload';
import ZipInfo from './ZipInfo';
import Translate from 'react-translate-component';


const FileUploadView = (props) => {
    const zipFileIcon = <i className="fas fa-file-archive mr-3"></i>;
    useEffect(() => {counterpart.setLocale(props.lang);}, [props]);

    return(
        <Container style={{ maxWidth: 550 }}>    
            <Card style={{ marginTop: 20 }}>
                <Card.Header>
                    {zipFileIcon} <Translate content="upload"/>
                </Card.Header>
                <Card.Body>
                    <FileUpload lang={props.lang}/>
                </Card.Body>
            </Card>
            <ZipInfo lang={props.lang}/>
        </Container>
    );
};

export default FileUploadView;
