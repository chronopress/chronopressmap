import React from 'react';
import Table from 'react-bootstrap/Table';
import { Card } from 'react-bootstrap';
import Translate from 'react-translate-component';

const ZipInfo = (props) => {
    return(
        <Card style={{ marginTop: 20 }}>
            <Card.Header>
                <Translate content="Header"/>
            </Card.Header>
            <Card.Body>
                <Translate content="bodyHeader"/>
                <ul>
                    <li><Translate content="description.files"/> </li>
                    <li><Translate content="description.meta"/> </li>
                </ul>
                <Translate content="metaHeader"/>
                <ul>
                    <li><Translate content="metaDesciption.file"/> <b>.meta.xlsx</b></li>
                    <li><Translate content="metaDesciption.excel"/> <b>A1</b> <Translate content="metaDesciption.key"/> <b>filename</b></li>
                    <li><Translate content="metaDesciption.row"/> </li>
                    <li><Translate content="metaDesciption.column"/> </li>
                </ul>
                <Table striped bordered hover size="sm">
                    <tbody>
                        <tr>
                            <th>
                                <Translate content="exampleName"/> 
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <Translate content="metaTable.file1"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Translate content="metaTable.file2"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Translate content="metaTable.metaName"/>
                            </td>
                        </tr>
                    </tbody>
                </Table>
                <br></br>
                <b><Translate content="metaExampleHeader"/></b>
                <Table striped bordered hover size="sm">
                    <tbody>
                        <tr>
                            <td>
                                <b>filename</b>
                            </td>
                            <td>
                                <Translate content="metaTable.auth"/>
                            </td>
                            <td>
                                <Translate content="metaTable.date"/>
                            </td>
                            <td>
                                <Translate content="metaTable.etc"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Translate content="metaTable.file1"/>
                            </td>
                            <td>
                              Oskar T.
                            </td>
                            <td>
                              2019-02-08
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Translate content="metaTable.file2"/>
                            </td>
                            <td>
                              Jadwiga S.
                            </td>
                            <td>
                              1989-02-08
                            </td>
                        </tr>
                    </tbody>
                </Table>
            </Card.Body>
        </Card>
    );
};

export default ZipInfo;
