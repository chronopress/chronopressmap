import React from 'react';
import { Form } from 'react-bootstrap';
import counterpart from 'counterpart';
import Translate from 'react-translate-component';



const FileUploadForm = props => {
    counterpart.setLocale(props.lang);
    const upload = counterpart.translate('uploadForm.upload');
    const select = counterpart.translate('uploadForm.select');
    const email = counterpart.translate('uploadForm.email');

    return (
        <Form onSubmit={props.onSubmit}>
            <Form.Label>
                <Translate content="uploadForm.label"/>
            </Form.Label>
            <div className="custom-file mb-4">
                <Form.Control
                    type="file"
                    className="custom-file-input"
                    id="customFile"
                    required
                    onChange={props.onFileChange}
                />
                <Form.Text className="text-muted">
                    <Translate content="uploadForm.labelAnnot"/>
                </Form.Text>
                <Form.Label 
                    data-browse={select} 
                    className="custom-file-label" 
                    htmlFor="customFile"
                >
                    {props.filename}
                </Form.Label>
            </div>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>
                    <Translate content="uploadForm.email"/>
                </Form.Label>
                <Form.Control
                    type="email"
                    required
                    onChange={props.onEmailChange}
                    placeholder={email}
                />
                <Form.Text className="text-muted">
                    <Translate content="uploadForm.emailAnnot"/>
                </Form.Text>
            </Form.Group>
            <Form.Control
                type="submit"
                value={upload}
                className="btn btn-primary btn-block"
            />
        </Form>
    );
};

export default FileUploadForm;
