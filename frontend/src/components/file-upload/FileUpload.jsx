import React, { Fragment, useState, useEffect } from 'react';
import FileUploadForm from './FileUploadForm';
import Message from '../Message';
import Progress from '../Progress';
import { api } from '../../api';
import { updateUploadPercentage } from '../../utils';
import counterpart from 'counterpart';
import Translate from 'react-translate-component';

const FileUpload = (props) => {
    const choose = <Translate content="uploadForm.uploadChoose"/>;
    const unknownError = <Translate content="errors.unknownError"/>;
    const serverError = <Translate content="errors.serverError"/>;
    const invalidForm = <Translate content="errors.invalidForm"/>;

    const [file, setFile] = useState('');
    const [filename, setFilename] = useState(choose);
    const [message, setMessage] = useState('');
    const [uploadPercentage, setUploadPercentage] = useState(0);
    const [email, setEmail] = useState('');

    useEffect(() => {counterpart.setLocale(props.lang);}, [props]);

    const onFileChange = e => {
        const choosedFile = e.target.files[0];
        if (choosedFile) {
            setFile(choosedFile);
            setFilename(choosedFile.name);
        }
    };

    const onEmailChange = e => {
        setEmail(e.target.value);
    };

    const resetUploader = () => {
        setFilename(choose);
        setFile('');
        setMessage('');
        setUploadPercentage(0);
    };

    const onSubmit = async e => {
        e.preventDefault();
        try {
            const response = await api.uploadFile(
                file, email, props.lang, updateUploadPercentage(setUploadPercentage));
            setMessage(response.data.message);
            setTimeout(resetUploader, 5000);
        } catch (err) {
            if (!err.response) {
                setMessage(unknownError);
                console.error(err);
            } else if (err.response.status === 500) {
                setMessage(serverError);
            } else {
                setMessage(invalidForm);
            }
        }
    };

    return (
        <Fragment>
            <FileUploadForm
                onFileChange={onFileChange}
                onEmailChange={onEmailChange}
                onSubmit={onSubmit}
                filename={filename}
                lang={props.lang}
            />
            <div style={{ marginTop: 10 }}>
                {uploadPercentage ? (
                    <Progress percentage={uploadPercentage} />
                ) : null}
            </div>
            <div style={{ marginTop: 10 }}>
                {message ? <Message msg={message} /> : null}
            </div>
        </Fragment>
    );
};

export default FileUpload;
