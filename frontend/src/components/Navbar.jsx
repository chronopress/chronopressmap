import React from 'react';
import { connect } from 'react-redux';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Translate from 'react-translate-component';


const NavBar = (props) => {
    const lang = <Translate content="langTitle"/>;
    const mainPage = <Translate content="mainPage"/>;

    return (
        <Navbar
            className="navbar-color"
            collapseOnSelect
            expand="lg"
            variant="light"
            bg="light"
        >
            <Navbar.Brand href="/">chronopressmap</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav>
                    <Nav.Link href="/">{mainPage}</Nav.Link>
                </Nav>
                <NavDropdown title={lang} onSelect={key => props.setLang(key)}>
                    <NavDropdown.Item eventKey="en">English</NavDropdown.Item>
                    <NavDropdown.Item eventKey="pl">Polski</NavDropdown.Item>
                </NavDropdown>
            </Navbar.Collapse>
        </Navbar>
    );
};

const mapStateToProps = state => {
    return {
        lang: state.lang,
    };
};


const mapDispatchToProps = dispatch => {
    return {
        setLang: (lang) => {
            dispatch({type: 'SET_LANG', lang: lang});
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
