import React from 'react';
import { connect } from 'react-redux';
import FileUploadView from './file-upload/FileUploadView';



const UploadPage = props => {
    return (
        <div>
            <FileUploadView lang={props.lang}/>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        lang: state.lang
    };
};

export default connect(mapStateToProps, {})(UploadPage);
