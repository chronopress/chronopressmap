import { blueIcon, goldIcon, orangeIcon, redIcon } from './js/leaflet-color-markers';


const Colour = {
    BLUE: '#3F92CF',
    GOLD: '#FDD326',
    ORANGE: '#CC862C',
    RED: '#CC2E41'
};

const HeatStageMultiplier = {
    FIRST_STAGE: 0.25,
    SECOND_STAGE: 1,
    THIRD_STAGE: 1.75
};

export function getPreferredIconAndColourForCount(count, avgCount) {
    if (count > Math.ceil(avgCount * HeatStageMultiplier.THIRD_STAGE)) {
        return [redIcon, Colour.RED];
    } else if (count > Math.ceil(avgCount * HeatStageMultiplier.SECOND_STAGE)) {
        return [orangeIcon, Colour.ORANGE];
    } else if (count > Math.ceil(avgCount * HeatStageMultiplier.FIRST_STAGE)) {
        return [goldIcon, Colour.GOLD];
    } else {
        return [blueIcon, Colour.BLUE];
    }
}
