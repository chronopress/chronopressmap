// source: https://github.com/pointhi/leaflet-color-markers

import L from 'leaflet';


let blueIcon = new L.Icon({
    iconUrl: require('../img/marker-icon-2x-blue.png'),
    shadowUrl: require('../img/marker-shadow.png'),
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

let goldIcon = new L.Icon({
    iconUrl: require('../img/marker-icon-2x-gold.png'),
    shadowUrl: require('../img/marker-shadow.png'),
    iconSize: [26, 43],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

let orangeIcon = new L.Icon({
    iconUrl: require('../img/marker-icon-2x-orange.png'),
    shadowUrl: require('../img/marker-shadow.png'),
    iconSize: [28, 47],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

let redIcon = new L.Icon({
    iconUrl: require('../img/marker-icon-2x-red.png'),
    shadowUrl: require('../img/marker-shadow.png'),
    iconSize: [30, 50],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

// let greenIcon = new L.Icon({
//     iconUrl: require('../img/marker-icon-2x-green.png'),
//     shadowUrl: require('../img/marker-shadow.png'),
//     iconSize: [25, 41],
//     iconAnchor: [12, 41],
//     popupAnchor: [1, -34],
//     shadowSize: [41, 41]
// });
//
// let yellowIcon = new L.Icon({
//     iconUrl: require('../img/marker-icon-2x-yellow.png'),
//     shadowUrl: require('../img/marker-shadow.png'),
//     iconSize: [25, 41],
//     iconAnchor: [12, 41],
//     popupAnchor: [1, -34],
//     shadowSize: [41, 41]
// });
//
// let violetIcon = new L.Icon({
//     iconUrl: require('../img/marker-icon-2x-violet.png'),
//     shadowUrl: require('../img/marker-shadow.png'),
//     iconSize: [25, 41],
//     iconAnchor: [12, 41],
//     popupAnchor: [1, -34],
//     shadowSize: [41, 41]
// });
//
// let greyIcon = new L.Icon({
//     iconUrl: require('../img/marker-icon-2x-grey.png'),
//     shadowUrl: require('../img/marker-shadow.png'),
//     iconSize: [25, 41],
//     iconAnchor: [12, 41],
//     popupAnchor: [1, -34],
//     shadowSize: [41, 41]
// });
//
// let blackIcon = new L.Icon({
//     iconUrl: require('../img/marker-icon-2x-black.png'),
//     shadowUrl: require('../img/marker-shadow.png'),
//     iconSize: [25, 41],
//     iconAnchor: [12, 41],
//     popupAnchor: [1, -34],
//     shadowSize: [41, 41]
// });

export {blueIcon, goldIcon, orangeIcon, redIcon};
