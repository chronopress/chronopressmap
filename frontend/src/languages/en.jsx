export default {
    upload: 'File upload',
    uploadForm:{
        label: 'Corpus',
        labelAnnot: 'Corpus file should be a zip archive',
        email: 'Email address',
        emailAnnot: 'We will send result link to this email address',
        uploadChoose: 'File',
        upload: 'Upload',
        select: 'Select file'
    },
    Header: 'ZIP file structure',
    bodyHeader:'Proper ZIP structure consists of:',
    description:{
        files: 'Files',
        meta: 'Metadata file, which describes each file',
    },
    metaHeader: 'Proper metadata file consist of',
    metaDesciption:{
        file:'Filename ends with',
        excel: 'Excel spreadsheet cell',
        key: 'contains key',
        row: 'First row contains metadata keys, which will be used as a filters',
        column:'First column contains filenames of files that are included in ZIP and should be proceeded, except metadata filename'
    },
    langTitle: 'Language',
    mainPage: 'Main page',
    exampleName: 'Example.zip',
    metaExampleHeader: 'Metadata file example:',
    metaTable:{
        metaName: 'example.meta.xlsx',
        auth: 'Author',
        date: 'Publication date',
        etc: 'etc.',
        file1: 'file1.txt',
        file2: 'file2.txt'
    },
    errors:{
        unknownError: 'Unknown Error',
        serverError: 'Server error',
        invalidForm: 'Missing required parameters'
    },
    filters: {
        update: 'Update'
    },
    editView: {
        cancel: 'Cancel',
        save: 'Save',
        edit: 'Edit',
        changedLocations: 'Changed locations'
    },
    map:{
        aggregationName: 'Aggregated locations',
        occurrences: 'occurrences',
        totalOccurrences: 'total occurrences',
        mostHits: 'Most hits'
    }
};
