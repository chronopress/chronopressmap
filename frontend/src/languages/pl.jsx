export default {
    upload: 'Przekazanie pliku',
    uploadForm:{
        label: 'Korpus',
        labelAnnot: 'Korpus musi być plikiem ZIP',
        email: 'Adres email',
        emailAnnot: 'Na podany adres email zostanie wysłany link do rezultatów',
        uploadChoose: 'Plik',
        upload: 'Załaduj',
        select: 'Wybierz plik'
    },
    Header: 'Struktura pliku ZIP',
    bodyHeader:'Poprawny plik ZIP musi zawierać:',
    description:{
        files: 'Pliki do przetworzenia',
        meta: 'Plik z metadanymi, w którym są zawarte informacje o każdym pliku',
    },
    metaHeader: 'Poprawna struktura pliku z metadanymi',
    metaDesciption:{
        file:'Nazwa pliku kończy się',
        excel: 'Komórka arkuszu Excel',
        key: 'zawiera klucz',
        row: 'Pierwszy wiersz arkuszu zawiera klucze, które zostaną wykorzystane jako filtry',
        column:'Pierwsza kolumna zawiera klucze, które odpowiadają nazwom plików, zawartych w pliku ZIP'
    },
    langTitle: 'Język',
    mainPage: 'Strona główna',
    exampleName: 'Plik.zip',
    metaExampleHeader: 'Przykład pliku z metadanymi:',
    metaTable:{
        metaName: 'przykład.meta.xlsx',
        auth: 'Autor',
        date: 'Data publikacji',
        etc: 'itd',
        file1: 'plik1.txt',
        file2: 'plik2.txt'
    },
    errors:{
        unknownError: 'Nieznany błąd',
        serverError: 'Wystąpił błąd serwera',
        invalidForm: 'Wymagane pola nie mogą być puste'
    },
    filters: {
        update: 'Zastosuj'
    },
    editView: {
        cancel: 'Anuluj',
        save: 'Zapisz',
        edit: 'Edytuj',
        changedLocations: 'Zmienione punkty'
    },
    map:{
        aggregationName: 'Zagregowane lokacje',
        occurrences: 'liczba wystąpień',
        totalOccurrences: 'całkowita liczba wystąpień',
        mostHits: 'Najwięcej trafień'
    }
};

