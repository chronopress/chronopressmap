import { getSelectedLocationsData } from './locationsFilter';

describe('filterLocations', () => {
    it('should return filtered locations', () => {
        const state = {
            result: {
                locations_aggregations: {},
                non_aggregable_locations: [],
                locations: [
                    'abcd',
                    'qwer',
                    'asdf',
                    'ertt'
                ]
            },
            locations: {
                'abcd': {
                    corpus_part_file: 'ab'
                },
                'qwer': {
                    corpus_part_file: 'fg'
                },
                'asdf': {
                    corpus_part_file: 'er',
                    is_center_preferred: false
                },
                'ertt': {
                    corpus_part_file: 'cd'
                }
            },
            selected: {'ab': true, 'er': true }
        };
        const expected = {
            locations_aggregations: state.result.locations_aggregations,
            non_aggregable_locations: ['asdf'],
            selectedLocationsIds: [
                'abcd',
                'asdf'
            ],
            selectedLocations: {
                'abcd': {
                    corpus_part_file: 'ab'
                },
                'asdf': {
                    corpus_part_file: 'er',
                    is_center_preferred: false
                }
            }
        };
        expect(getSelectedLocationsData(state)).toEqual(expected);
    });
});