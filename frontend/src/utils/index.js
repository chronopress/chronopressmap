export const updateUploadPercentage = setUploadPercentageCb => progressEvent => {
    const percentage = Math.round(
        (progressEvent.loaded * 100) / progressEvent.total
    );
    setUploadPercentageCb(parseInt(percentage));
};


export const omitEmptyValues = obj => {
    return Object.keys(obj).reduce((result, key) => {
        const value = obj[key];
        if(!value)
            return result;
        if ((Array.isArray(value) || typeof value === 'string') && value.length === 0){
            return result;
        }
        result[key] = value;
        return result;
    }, {});
};
