

export const getSelectedLocationsData = (state) => {

    let selectedLocationsIds = [];
    let selectedLocations = {};
    let non_aggregable_locations = [];
    let locations_aggregations = {};  
    
    if(state.result && state.selected){
        const locationsIds = state.result.locations;
        const locations = state.locations;
        const selectedFiles = state.selected;
        
        locationsIds.forEach(locationId => {
            const location = locations[locationId];
            const fileId = location.corpus_part_file;
    
            if(selectedFiles[fileId]){
                selectedLocationsIds.push(locationId);
                selectedLocations[locationId] = location;
    
                if (location.is_center_preferred === false) {
                    non_aggregable_locations.push(locationId);
                }
            }
        });

        locations_aggregations = state.result.locations_aggregations;
    }

    return  {
        locations_aggregations,
        selectedLocationsIds,
        selectedLocations,
        non_aggregable_locations
    };
};