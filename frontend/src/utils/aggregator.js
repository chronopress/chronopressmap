import { generateUUID } from './helpers';


export const ZoomThreshold = {
    FIRST_AGGREGATION: 10, // First aggregation is preferred over second and third
    SECOND_AGGREGATION: 11,
    THIRD_AGGREGATION: 10,
    FOURTH_AGGREGATION: 9,
    FIFTH_AGGREGATION: 8,
    SIXTH_AGGREGATION: 7,
    SEVENTH_AGGREGATION: 5,
    EIGHTH_AGGREGATION: 3,
    NINTH_AGGREGATION: 2,
    MAX_AGGREGATION: 0
};

const AggregationsLevels = {
    FIRST_AGGREGATION: 9,
    SECOND_AGGREGATION: 8,
    THIRD_AGGREGATION: 7,
    FOURTH_AGGREGATION: 6,
    FIFTH_AGGREGATION: 5,
    SIXTH_AGGREGATION: 4,
    SEVENTH_AGGREGATION: 3,
    EIGHTH_AGGREGATION: 2,
    NINTH_AGGREGATION: 1,
    MAX_AGGREGATION: 0
};

function calculateAverageCount(locations) {
    if (locations) {
        let totalCounts = 0;
        for (let i = 0; i < locations.length; ++i) {
            totalCounts += locations[i].count;
        }
        return Math.floor(totalCounts / locations.length);
    }
    return 0;
}

function reduceLocations(locations) {
    let reducedLocations = {};
    if (locations.length) {
        reducedLocations = locations.reduce((result, location) => {
            if(result[location.name]){
                result[location.name].count += location.count;
            } else {
                result[location.name] = location;
            }
            return result;
        }, {});
    }

    return Object.values(reducedLocations);
}

function getNonAggregatedLocations(locationsData, locationIDs) {
    const selectedLocations = locationsData.non_aggregable_locations.map(location => {
        let matchedLocation = locationsData.selectedLocations[location];
        locationIDs.push(matchedLocation._id);
        return matchedLocation;
    });
    return reduceLocations(selectedLocations);
}

function reduceUnselectedLocationsFromAggregation(locationsData) {
    locationsData.locations_aggregations.forEach(aggregationLevel => {
        aggregationLevel.aggregations.forEach(aggregation => {
            aggregation.locations = aggregation.locations.filter(
                (location) => locationsData.selectedLocations[location]
            );
        });
    });
    return locationsData;
}

function getAggregationInfo(aggregation, selectedLocations) {
    let mostCommonLocation = null;
    let totalOccurrencesCount = 0;
    const mappedLocations =  aggregation.locations.map(locationID => {
        return selectedLocations[locationID];
    });

    const reducedLocations = reduceLocations(JSON.parse(JSON.stringify(mappedLocations)));
    reducedLocations.forEach(location => {
        totalOccurrencesCount += location.count;
        if (!mostCommonLocation) {
            mostCommonLocation = location;
        } else if (mostCommonLocation.count < location.count) {
            mostCommonLocation = location;
        }
    });

    return {
        name: 'Aggregated locations',
        _id: generateUUID(),
        geometry: aggregation.geometry,
        aggregatedLocationsCount: reducedLocations.length,
        count: totalOccurrencesCount,
        mostCommonLocation: mostCommonLocation.name,
        mostOccurrences: mostCommonLocation.count
    };
}

function getLocationsFromAggregation(locationsAggregation, selectedLocations, minAggregationLevel, locationIDs) {
    let locations = [];
    locationsAggregation.aggregations.forEach(aggregation => {
        const aggregatedLocationsCount = aggregation.locations.length;
        if (aggregatedLocationsCount) {
            if (locationsAggregation.level === minAggregationLevel) {
                aggregation.locations.forEach(locationID => {
                    let location = selectedLocations[locationID];
                    if (location.is_center_preferred) {
                        location.geometry = aggregation.geometry;
                    }
                    locationIDs.push(location._id);
                    locations.push(location);
                });
            } else {
                const aggregationInfo = getAggregationInfo(aggregation, selectedLocations);
                locationIDs.push(aggregationInfo._id);
                locations.push(aggregationInfo);
            }
        }
    });

    if (locationsAggregation.level === minAggregationLevel) {
        locations = reduceLocations(locations);
    }
    return locations;
}

function createLocationsDisplay(locationIDs) {
    let locationsDisplay = {};
    locationIDs.forEach(locationID => {
        locationsDisplay[locationID] = true;
    });
    return locationsDisplay;
}

export function getSelectedLocationsArray(selectedLocations){
    return Object.keys(selectedLocations).map(id => selectedLocations[id]);
}

export function zoomToAggregationMapping(zoom) {
    let aggregationLevel = AggregationsLevels.MAX_AGGREGATION;
    if (zoom >= ZoomThreshold.FIRST_AGGREGATION) {
        aggregationLevel = AggregationsLevels.FIRST_AGGREGATION;
    } else if (zoom >= ZoomThreshold.SECOND_AGGREGATION) {
        aggregationLevel = AggregationsLevels.SECOND_AGGREGATION;
    } else if (zoom >= ZoomThreshold.THIRD_AGGREGATION) {
        aggregationLevel = AggregationsLevels.THIRD_AGGREGATION;
    } else if (zoom >= ZoomThreshold.FOURTH_AGGREGATION) {
        aggregationLevel = AggregationsLevels.FOURTH_AGGREGATION;
    } else if (zoom >= ZoomThreshold.FIFTH_AGGREGATION) {
        aggregationLevel = AggregationsLevels.FIFTH_AGGREGATION;
    } else if (zoom >= ZoomThreshold.SIXTH_AGGREGATION) {
        aggregationLevel = AggregationsLevels.SIXTH_AGGREGATION;
    } else if (zoom >= ZoomThreshold.SEVENTH_AGGREGATION) {
        aggregationLevel = AggregationsLevels.SEVENTH_AGGREGATION;
    } else if (zoom >= ZoomThreshold.EIGHTH_AGGREGATION) {
        aggregationLevel = AggregationsLevels.EIGHTH_AGGREGATION;
    } else if (zoom >= ZoomThreshold.NINTH_AGGREGATION) {
        aggregationLevel = AggregationsLevels.NINTH_AGGREGATION;
    }

    return aggregationLevel;
}

export function splitAggregationData(locationsData) {
    const reducedLocationsData = reduceUnselectedLocationsFromAggregation(JSON.parse(JSON.stringify(locationsData)));
    let locationIDs = [];

    // Extracting aggregated locations
    const minAggregationLevel = reducedLocationsData.locations_aggregations.length - 1;
    let selectedAggregations = {};
    reducedLocationsData.locations_aggregations.forEach(aggregationLevel => {
        selectedAggregations[aggregationLevel.level] = getLocationsFromAggregation(aggregationLevel,
            reducedLocationsData.selectedLocations, minAggregationLevel, locationIDs);
    });

    // Extracting non-aggregated locations
    const nonAggregatedLocations = getNonAggregatedLocations(locationsData, locationIDs);
    const avgCount = calculateAverageCount(
        nonAggregatedLocations.concat(selectedAggregations[minAggregationLevel]));
    const locationsVisibility = createLocationsDisplay(locationIDs);

    return {
        nonAggregatedLocations: nonAggregatedLocations,
        selectedAggregations: selectedAggregations,
        avgCount: avgCount,
        locationsVisibility: locationsVisibility
    };
}
