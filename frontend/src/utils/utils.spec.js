import { omitEmptyValues } from './index';

describe('testing omitEmptyValues', () => {
    it('should return object without empty values', () => {
        const obj = {
            key_a: 'a',
            key_b: '',
            key_c: ['a', 'b'],
            key_d: []
        };
        expect(omitEmptyValues(obj)).toEqual({key_a: 'a', key_c: ['a', 'b']});
    });
});
