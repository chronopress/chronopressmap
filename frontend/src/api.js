import axios from 'axios';

export const api = {
    uploadFile: async (file, email, lang, onUploadProgress) => {
        const formData = new FormData();
        formData.append('file', file);
        formData.append('email', email);
        formData.append('lang', lang);
        return await axios.post('upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            onUploadProgress,
        });
    },
    fetchResult: async (uuid) => {
        return await axios.get('/result', {
            params: {
                'uuid': uuid
            }
        });
    },
    editLocations: async (changedLocations, result_uuid, lang) => {
        return await axios.post('/edit_locations', {locations: changedLocations, result_uuid, lang});
    }
};
