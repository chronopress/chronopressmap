import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
import UploadPage from './components/UploadPage';
import ResultView from './components/ResultView';
import { rootReducer }  from './reducers';
import counterpart from 'counterpart';
import en from './languages/en';
import pl from './languages/pl';
import  Navbar  from './components/Navbar';

counterpart.registerTranslations('en', en);
counterpart.registerTranslations('pl', pl);

const store = createStore(rootReducer, 
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const App = () => {
    return (
        <Provider store={store}>
            <Navbar/>
            <Router>
                <Switch>
                    <Route exact path="/" component={UploadPage}/>
                    <Route path="/result/:uuid" component={ResultView}/>
                    <Route path="/result" component={Redirection}/>
                </Switch>
            </Router>
        </Provider>
    );
};

const Redirection = () => {
    return (
        <Redirect to="/" />
    );
};


export default App;
