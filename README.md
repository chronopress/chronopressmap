# chronopressmap

[[_TOC_]]

## Docker setup

### Prerequisites

* git-lfs
* Python 3.7
* node
* [yarn](https://classic.yarnpkg.com/en/docs/install) (or npm)
* [docker-compose](https://docs.docker.com/get-docker/)
* [docker](https://docs.docker.com/compose/install/)
* MongoDB
* redis
* .env

### Run

First-time-only init, required for git-lfs

```shell
git-lfs install && git-lfs pull
```

User should have locally created .env file with ports assignments for CHRONOPRESS_BACKEND_PORT and CHRONOPRESS_FRONTEND_PORT.

Example of .env file is provided via repo

```shell
cp .env.docker.example .env
```
**In order to enable e-mail sending feature, user should provide MAIL_ADDRESS and MAIL_PASSWORD via .env file.**

Start containers

```shell
cd chronopressmap
source .env
sudo docker-compose up
```

### Rebuild images

Required to apply src code changes

```shell
sudo docker-compose up --build
```

## Standard setup

### Backend

#### Local Setup

##### Virtual env

```shell
cd backend
python3 -m venv venv
sudo ./install_wraplem_dependencies.sh
source venv/bin/activate
```

##### Install Backend Requirements

```shell
pip install -r requirements.txt
```

or with [poetry](https://python-poetry.org/)

```shell
poetry install
```

##### Start Server

```shell
sudo ./start_server.sh
```

##### Cleanup wraplem dependencies

```shell
sudo ./cleanup_wraplem_dependencies.sh
```

### Frontend

#### Install Frontend Requirements

```shell
cd frontend
yarn install
```

#### Start App

```shell
yarn start
```

### Development

frontend tests/linter:

```shell
cd frontend
yarn test
yarn lint
```

backend tests/linter:

make sure `tox` is installed (`pip install tox`)

```shell
cd backend
tox -e test
tox -e lint
```

backend logs:

All backend-related logs can be found in **../backend/logs** directory.

memory usage tracking:

```shell
sudo docker ps -a # select container and copy its CONTAINER ID
sudo docker exec -it ${CONTAINER_ID} ./log_memory_usage.sh
```

running memory tacking in detached mode:
```shell
sudo docker ps -a # select container and copy its CONTAINER ID
sudo docker exec -it ${CONTAINER_ID} ./log_memory_usage.sh &
```

Memory usage logs can be found in **../backend/logs** directory.

## Database

Example result record:

```json
{
    "uuid" : "73043b808ea64b458cac68be1f86d737",
    "date" : "2020-04-15T21:39:28.871Z",
    "corpus_name" : "corpus_name",
    "corpus_files" : [
        {
            "filename" : "dummy_corpus_filename",
            "categories" : [
                "prl",
                "sport"
            ],
            "locations" : [
                {
                    "name" : "wroclaw",
                    "coordinates" : {
                        "latitude" : 51.1079,
                        "longitude" : 17.0385
                    },
                    "count": 1337
                },
                {
                    "name" : "katowice",
                    "coordinates" : {
                        "latitude" : 50.2649,
                        "longitude" : 19.0238
                    },
                    "count": 42
                }
            ]
        }
    ]
}
```

## Zip file structure

User should upload a **zip** file, which should contain:
* at least 1 file with **.txt** or **.docx** extension
* only 1 metadata file which contains information for each file, which ends with **.meta.xlsx**

Example:
```
corpus.zip
    ├── corpus.meta.xlsx
    └── file.txt
```

Zip file might contain numerous files:
```
corpus.zip/
├── file_1.txt
├── file_2.docx
└── corpus.meta.xlsx
```

### Metadata

Each file should be described with it's metadata.

**.xlsx** file structure should be the following:
* first cell of the sheet should contain key **filename**
* the rest of the first row cells defines file attributes, which will be used as a filters

|Filename|Journal title|Article title|Authors|Publication date|Exposition|Medium|Period|Status|Style|
|:-------|-------------|-------------|-------|----------------|----------|------|------|------|-----|

Example:

|Filename|Journal title|Article title|Authors|Publication date|Exposition|Medium|Period|Status|Style|
|:-------|-------------|-------------|-------|----------------|----------|------|------|------|-----|
|file_1.txt|Przyjaciółka|Ja nie zapomniałam|Jadwiga S.|1953-03-29|Prasa drukowana|tygodnik|Obieg oficjalny krajowy||język prasy|
|file_2.docx|Przegląd Sportowy|Młodość świata defilowała na Łużnikach||1957-07-29|1|paper|d|1_obieg|press|


### MongoDB - dump and restore

To create a dump of the db into archive (container should be active):

```bash
docker exec <id of monog db container> sh -c 'mongodump --archive' > <path to dump>
```

To restore a dump of the db from archive:

```bash
docker exec -i <id of monog db container> sh -c 'mongorestore --archive' < <path to dump>
```

MongoDB container id could be extracted with a help of the following:

```bash
docker ps --filter "name=mongodb" -q
```
Above command will return the id of active images with name=**mongodb**


Example:
```bash
docker ps --filter "name=mongodb" -q
3bfed0546efe

docker exec 3bfed0546efe sh -c 'mongodump --archive' > backend/tmp/db.dump

docker exec -i 3bfed0546efe sh -c 'mongorestore --archive' < backend/tmp/db.dump
```

Example dump contains processed dataset under **UUID** - f9659dcad8464d3498c25c4e7195c98c.

Results of 'db.dump' can be seen after under - http://localhost:3000/result/f9659dcad8464d3498c25c4e7195c98c

Results of 'covid_small.dump' can be seen after under - http://localhost:3000/result/0ff535f4d6614dffbbb0ed16c8c2f371


### External sources
[Icon source](https://github.com/pointhi/leaflet-color-markers)
[Ner](https://github.com/CLARIN-PL/PolDeepNer/)
[Lemmatizer](https://gitlab.clarin-pl.eu/nlpworkers/ner2json/tree/master)

