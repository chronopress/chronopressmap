#!/bin/bash

rm -rf /usr/local/share/corpus2
rm -rf /usr/local/share/polem
rm -rf /usr/local/lib/wrap_lem

venv_libs=$((echo "from distutils.sysconfig import get_python_lib" ; echo "print(get_python_lib())") | venv/bin/python)

rm $venv_libs/WrapLem.py $venv_libs/_WrapLem.so

exit 0
