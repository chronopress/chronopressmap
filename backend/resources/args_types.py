from email_validator import EmailNotValidError, validate_email

from lang.responses import LANGUAGES
from logger import logger
from utils import zip_utils


def email_type(email_str):
    logger.debug('Validating e-mail address.')
    try:
        v = validate_email(email_str)
        logger.debug('E-mail validated.')
        return v['email']
    except EmailNotValidError as e:
        raise ValueError(e)


def zipfile_type(file):
    logger.debug('Checking zipfile type.')
    if zip_utils.validate_zip_format(file.stream._file):
        logger.debug('Zipfile is valid.')
        file.stream.seek(0)
        return file
    raise ValueError('File is not valid ZIP archive')


def lang_type(lang_str):
    logger.debug('Validating language.')
    if lang_str in LANGUAGES:
        return lang_str
    raise ValueError('Invalid language passed.')
