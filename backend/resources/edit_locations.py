from flask_restful import Resource, reqparse

from db import Location, Point, Result
from db.new_location import NewLocation
from lang.responses import LANG_RESPONSES
from logger import logger
from tasks.process_corpus import make_locations_aggregations

from .args_types import lang_type


class EditLocations(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('lang', type=lang_type, required=True)
    parser.add_argument('locations', type=dict, action='append', required=True)
    parser.add_argument('result_uuid', type=str, required=True)

    def post(self):
        data = self.parser.parse_args()
        locations = self.to_locations(data['locations'])
        result_uuid = data['result_uuid']
        try:
            self.update_locations(locations)
            self.reaggregate(result_uuid)
            return {'message': LANG_RESPONSES['updated_locs'][data['lang']]}, 200

        except Exception as e:
            logger.error('[LOCATIONS EDITOR] %s', e)
        return {'message': LANG_RESPONSES['failed_update_locs'][data['lang']]}, 400

    @staticmethod
    def to_locations(array_of_locations):
        return [NewLocation(**location) for location in array_of_locations]

    @staticmethod
    def update_locations(new_locations):
        for new_location in new_locations:
            Location.objects(uuid=new_location.uuid).update(
                geometry=Point(
                    geo_json=[new_location.center_lng, new_location.center_lat]
                ),
                center_lat=new_location.center_lat,
                center_lng=new_location.center_lng,
            )

    @staticmethod
    def reaggregate(uuid):
        locations = Result.objects(uuid=uuid).get().locations
        aggregated, non_aggregated = make_locations_aggregations(locations)
        Result.objects(uuid=uuid).update(
            locations_aggregations=aggregated, non_aggregable_locations=non_aggregated
        )
