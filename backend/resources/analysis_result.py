from bson import json_util
import json

from flask import request, Response
from flask_restful import Resource

import db
from constants import HTTP_SUCCESS_RESPONSE_CODE


class AnalysisResult(Resource):
    def get(self):
        experiment_result = db.Result.objects(uuid=request.args['uuid']).first()
        corpus_part_files = self._get_corpus_part_files(experiment_result)
        locations = self._get_locations(experiment_result)
        result_json = {'result': experiment_result.to_mongo(),
                       'corpus_part_files': corpus_part_files,
                       'locations': locations
                       }

        return Response(json.dumps(result_json, default=json_util.default),
                        status=HTTP_SUCCESS_RESPONSE_CODE,
                        mimetype='application/json')

    def _get_corpus_part_files(self, experiment_result):
        return {corpus_part_file.uuid: corpus_part_file.to_mongo()
                for corpus_part_file in experiment_result.corpus_part_files}

    def _get_locations(self, experiment_result):
        return {location.uuid: location.to_mongo() for location in experiment_result.locations}
