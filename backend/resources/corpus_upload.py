from dataclasses import asdict

from flask_restful import Resource, reqparse

from constants import HTTP_SUCCESS_RESPONSE_CODE
from lang.responses import LANG_RESPONSES
from tasks import process_corpus, send_mail
from utils.common import save_file_with_uuid

from .args_types import email_type, zipfile_type, lang_type


class CorpusUpload(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('file',
                        type=zipfile_type,
                        location='files',
                        required=True)
    parser.add_argument('email', type=email_type, required=True)
    parser.add_argument('lang', type=lang_type, required=True)

    def post(self):
        data = self.parser.parse_args()
        file = data['file']
        corpus = save_file_with_uuid(file)
        corpus.lang = data['lang']
        corpus.email = data['email']
        process_corpus.apply_async((asdict(corpus),), link=send_mail.s(data['email'], corpus.lang))
        return {'message': LANG_RESPONSES['uploaded'][corpus.lang]}, HTTP_SUCCESS_RESPONSE_CODE
