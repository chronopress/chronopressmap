from abc import abstractmethod, ABC
from dataclasses import dataclass
from typing import Dict

from geo_location.geo_code.geo_code_access_interface import GeoCodeAccessInterface


class GeoCodeProviderException(Exception):
    pass


class GeoCodeProviderInterface(ABC):
    @abstractmethod
    def get_geo_codes(self, names_with_count: Dict[str, int]) -> Dict[str, 'GeoCodeProviderInterface.GeoCode']:
        pass

    @dataclass
    class GeoCode(object):
        name: str
        count: int
        geo_code_data: GeoCodeAccessInterface.GeoCodeData
        is_valid: bool = True

        def __str__(self):
            if not self.is_valid:
                return f'Name: {self.name} is invalid'
            return f'Name: {self.name}, count: {self.count}, geo_code_data: {self.geo_code_data}'

        def __repr__(self):
            return f'GeoCode(name: {self.name!r}, count: {self.count!r}, geo_code_data: {self.geo_code_data!r}, ' \
                   f'is_valid: {self.is_valid!r})'
