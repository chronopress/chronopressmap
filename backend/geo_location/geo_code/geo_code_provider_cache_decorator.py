from dataclasses import asdict
from typing import Dict

from geo_location.geo_code.geo_code_provider_interface import GeoCodeProviderInterface
from geo_location.geo_code.geo_code_access_interface import GeoCodeAccessInterface

from logger import logger


class GeoCodeProviderCacheDecorator(GeoCodeProviderInterface):
    def __init__(self, geo_code_provider: GeoCodeProviderInterface, cache_handle):
        self._geo_code_provider = geo_code_provider
        self.cache_handle = cache_handle

    def cache_geo_codes(self, geo_codes):
        logger.debug('Caching geo codes')
        for name, geo_code in geo_codes.items():
            if geo_code.is_valid:
                self.cache_handle.set(name, asdict(geo_code.geo_code_data))
        logger.debug('Cashing finished.')

    def get_geo_codes(self, names_with_count: Dict[str, int]) -> Dict[str, 'GeoCodeProviderInterface.GeoCode']:
        logger.debug('Getting geo codes')
        left_names_with_count = {}
        cache_result = {}
        for name, count in names_with_count.items():
            logger.debug('Checking for cached results')
            cached_geo_code = self.cache_handle.get(name)
            if cached_geo_code:
                logger.debug(f'Cache hit: {name}, center_lng: {cached_geo_code["center_lng"]}, '
                             f'center_lat: {cached_geo_code["center_lat"]}')
                cache_result[name] = _make_geo_code(name, count, cached_geo_code)
            else:
                logger.debug(f'Cache miss: {name}')
                left_names_with_count[name] = count
        if len(left_names_with_count) > 0:
            logger.debug('Getting geo codes for names which were not found in cache')
            resp = self._geo_code_provider.get_geo_codes(left_names_with_count)
            self.cache_geo_codes(resp)
            result = {**cache_result, **resp}
        else:
            logger.debug('Cached results found.')
            result = cache_result

        return result


def _make_geo_code(name, count, geo_code_data_dict):
    geo_code_data = GeoCodeAccessInterface.GeoCodeData(**geo_code_data_dict)
    return GeoCodeProviderInterface.GeoCode(name, count, geo_code_data)
