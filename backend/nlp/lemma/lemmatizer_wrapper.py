from typing import Dict

import WrapLem

import utils.zip_utils as zip_utils

from logger import logger

from nlp.lemma.clarinpl.ner2json import getAnnotations


class LemmatizerException(Exception):
    pass


class LemmatizerWrapper(object):
    LOCATION_CATEGORIES = {
        'nam_loc',
        'nam_loc_country_region',
        'nam_loc_gpe_admin1',
        'nam_loc_gpe_admin2',
        'nam_loc_gpe_admin3',
        'nam_loc_gpe_city',
        'nam_loc_gpe_conurbation',
        'nam_loc_historical_region'
    }

    def __init__(self):
        self.lemmatizer = WrapLem.CascadeLemmatizer.assembleLemmatizer()

    def lemmatize_locations(self, ner_processed_output_zip) -> Dict[str, Dict[str, int]]:
        """
        Uses clarinpl lemmatizer to get lemmas from NER output files

        :param ner_processed_output_zip: path to zip file containing output from NER processing
        :return: dictionary mapping single text file name to dict of location lemmas with occurrence count
        """

        logger.debug('Lemmatizing locations.')
        if not zip_utils.validate_zip_format(ner_processed_output_zip):
            raise LemmatizerException('Invalid input file, file is not zip archive')

        lemmas = {}
        for name, ner_data in zip_utils.iterate_zip_archive(ner_processed_output_zip):
            lemmas[name] = self._get_lemmas(ner_data)

        logger.debug('Lemmatized locations: {}'.format(lemmas))
        return lemmas

    def _get_lemmas(self, ner_data_file_handle):
        try:
            return self._extract_lemmas(getAnnotations(ner_data_file_handle, self.lemmatizer))
        except Exception as e:
            raise LemmatizerException('Lemmatizer could not process input properly. Error: ' + str(e))

    def _extract_lemmas(self, annotations):
        return {annotation.lemma: annotation.count for annotation in annotations
                if annotation.category in LemmatizerWrapper.LOCATION_CATEGORIES}
