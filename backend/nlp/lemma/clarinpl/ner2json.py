"""
This file comes from https://gitlab.clarin-pl.eu/nlpworkers/ner2json/-/blob/master/src/ner2json.py
it was slightly modified
"""

import lxml.etree as ET


class Token:

    def __init__(self, orth, base, ctag):
        self.orth = orth
        self.base = base
        self.ctag = ctag

    def get_orth(self):
        return self.orth

    def get_base(self):
        return self.base

    def get_ctag(self):
        return self.ctag


class Annotation:

    def __init__(self, category, tokens):
        self.category = category
        self.tokens = tokens
        self.lemma = self.get_orth()
        self.count = 1

    def get_count(self):
        return self.count

    def inc(self):
        self.count = self.count + 1

    def toDict(self):
        return {"keyword": self.lemma, "type": self.category, "count": self.count}

    def get_category(self):
        return self.category

    def get_tokens(self):
        return self.tokens

    def get_orth(self):
        return " ".join([token.get_orth() for token in self.tokens])

    def get_base(self):
        return " ".join([token.get_base() for token in self.tokens])

    def get_ctag(self):
        return " ".join([token.get_ctag() for token in self.tokens])

    def get_space(self):
        return " ".join(["True" for token in self.tokens])

    def get_lemma(self):
        return self.lemma

    def set_lemma(self, lemma):
        self.lemma = lemma

    def __str__(self):
        return "[%s] %s" % (self.get_category(), self.get_lemma())


def sentence_ner(sentence, lemmatizer):
    channels = {}
    for token in sentence.iter("tok"):
        orth = token.find("./orth").text
        base = token.find("./lex/base").text
        ctag = token.find("./lex/ctag").text
        t = Token(orth, base, ctag)
        for channel in token.iter("ann"):
            index = int(channel.text)
            chan = channel.attrib["chan"]

            if index > 0 and chan.startswith("nam_"):
                channels.setdefault(chan, {}) \
                    .setdefault(index, []) \
                    .append(t)

    annotations = []
    for (ann_type, group) in channels.items():
        for tokens in group.values():
            if ann_type == 'nam_adj':
                continue
            an = Annotation(ann_type, tokens)
            lemma = lemmatizer.lemmatizeS(an.get_orth(), an.get_base(), an.get_ctag(), an.get_space(),
                                          an.get_category(), False)
            # to remove too short ne
            lemma_len = len(lemma)
            if lemma_len < 3:
                continue
            if lemma_len < 4 and " " in lemma:
                continue

            an.set_lemma(lemma)
            annotations.append(an)

    return annotations


def ccl_ner(ccl, lemmatizer):
    tree = ET.parse(ccl)
    annotations = []

    for sentence in tree.iter("sentence"):
        annotations += sentence_ner(sentence, lemmatizer)
    return annotations


def count_annotations(items):
    counts = dict()
    for i in items:
        val = str(i)
        if val in counts:
            counts[val].inc()
        else:
            counts[val] = i

    # res=sorted(counts, key=counts.get, reverse=True)
    res = sorted(counts.values(), key=lambda x: x.get_count(), reverse=True)
    return res


def getAnnotations(ccl, lemmatizer):
    annotations = ccl_ner(ccl, lemmatizer)
    return count_annotations(annotations)
