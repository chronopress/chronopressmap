from dataclasses import dataclass, field
from typing import Dict, List

from box import Box

from geo_location.geo_code.geo_code_provider_interface import GeoCodeProviderException, GeoCodeProviderInterface

from nlp.lemma.lemmatizer_wrapper import LemmatizerException

from nlp.ner.ner_access import NerAccessException

from logger import logger


class NlpChain(object):
    @dataclass
    class NlpChainReturn(object):
        success: bool = False
        msg: str = ''
        text_geo_codes: Dict[str, List[GeoCodeProviderInterface.GeoCode]] = field(default_factory=dict)

    def __init__(self):
        box = Box()
        self.ner_access = box.ner_access
        self.lemmatizer_wrapper = box.lemmatizer_wrapper
        self.geo_code_provider = box.geo_code_provider

    def run_nlp_processing_chain(self, path_to_corpus_zip: str) -> 'NlpChain.NlpChainReturn':
        """
        Assembles particular NLP steps into full NLP chain transforming corpuses into geo locations names

        :param path_to_corpus_zip: path to zip file containing corpuses
        :return: (NlpChain.NlpChainReturn) Response object containing status, msg and dictionary with geo locations for
        each text in corpus
        """

        logger.debug('Running nlp processing chain.')
        try:
            ner_output_zip_path = self.ner_access.process_corpus(path_to_corpus_zip)
            files_lemmas = self.lemmatizer_wrapper.lemmatize_locations(ner_output_zip_path)
            text_geo_codes = self._get_geo_codes_for_each_text_in_corpus(files_lemmas)
        except (NerAccessException, LemmatizerException, GeoCodeProviderException) as ex:
            logger.error(f'NLP processing chain failed: {ex}')
            return NlpChain.NlpChainReturn(False, str(ex))

        logger.debug('Nlp processing chain finished.')
        return NlpChain.NlpChainReturn(True, "SUCCESS", text_geo_codes)

    def _get_geo_codes_for_each_text_in_corpus(self, files_lemmas):
        logger.debug('Trying to get geo codes for each text in corpus.')
        text_geo_codes = {}
        for original_file_name, lemmas in files_lemmas.items():
            name_geo_codes = self.geo_code_provider.get_geo_codes(lemmas)
            text_geo_codes[original_file_name] = [geo_code for geo_code in name_geo_codes.values() if geo_code.is_valid]
        logger.debug('Geo codes received.')
        return text_geo_codes
