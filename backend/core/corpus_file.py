from dataclasses import dataclass


@dataclass
class Corpus():
    uuid: str
    cwd: str
    path: str
    file: str
    lang: str = None
    email: str = None
    meta: dict = None
    categories: dict = None
