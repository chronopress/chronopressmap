import os

import db
from box import Box
from core.corpus_file import Corpus
from core.metadata import load_metadata
from logger import logger
from nlp.nlp_chain import NlpChain
from tasks.celeryapp import celery
from utils import mailing_service
from utils.zip_utils import prepare_corpus_zip


@celery.task
def send_mail(uuid, mail_address, lang, msg_type=None, filename=None):
    if uuid is None:
        if msg_type is not None and filename is not None:
            mailing_service.send_failed_analysis_result(
                mail_address, lang, msg_type, filename
            )
        return

    mailing_service.send_analysis_result(uuid, mail_address, lang)


def cleanup(corpus_zip):
    logger.debug('removing user data')
    os.remove(corpus_zip.meta)
    os.remove(corpus_zip.path)


@celery.task
def process_corpus(corpus):
    corpus = Corpus(**corpus)
    corpus_zip = prepare_corpus_zip(corpus)
    if not corpus_zip.status:
        return send_mail(None, corpus.email, corpus.lang, 'no_meta', corpus.file,)

    corpus.meta = load_metadata(corpus_zip.meta)
    if corpus.meta is None:
        return send_mail(None, corpus.email, corpus.lang, 'invalid_meta', corpus.file,)

    nlp_chain = NlpChain()
    nlp_result = None
    try:
        nlp_result = nlp_chain.run_nlp_processing_chain(corpus_zip.path)
    except Exception as e:
        logger.error('[NLP CHAIN] - %s', e)
    if nlp_result is not None and nlp_result.success:
        result = make_result(nlp_result, corpus)
        uuid = save_result(result)
        cleanup(corpus_zip)
        return uuid
    return send_mail(None, corpus.email, corpus.lang, 'nlp_failed', corpus.file,)


def make_result(nlp_result, corpus):
    corpus_part_files, locations = make_corpus_files_and_locations(nlp_result, corpus)
    aggregations, non_aggregable = make_locations_aggregations(locations)

    return db.Result(
        corpus_name=corpus.file,
        corpus_part_files=corpus_part_files,
        all_categories=corpus.meta.categories,
        locations_aggregations=aggregations,
        non_aggregable_locations=non_aggregable,
        locations=locations,
    )


def make_corpus_files_and_locations(nlp_result, corpus):
    corpus_part_files = []
    locations = []
    for corpus_filename, geocodes in nlp_result.text_geo_codes.items():
        corpus_part_file = db.CorpusPartFile(
            filename=corpus_filename, categories=corpus.meta[corpus_filename]
        )
        locations.extend(
            [
                db.Location.from_geocode(geocode, corpus_part_file)
                for geocode in geocodes
            ]
        )

        corpus_part_files.append(corpus_part_file)

    return corpus_part_files, locations


def make_locations_aggregations(locations):
    box = Box()
    return box.locations_aggregator.aggregate(locations)


def save_result(result):
    for corpus_part_file in result.corpus_part_files:
        logger.debug(f'corpus_part_file UUID: {corpus_part_file.uuid}')
        corpus_part_file.save()

    for location in result.locations:
        logger.debug(f'location UUID: {location.uuid}')
        location.save()

    logger.info('result UUID: {}'.format(result.uuid))
    result.save()

    return result.uuid
