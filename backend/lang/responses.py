LANGUAGES = ['pl', 'en']

LANG_RESPONSES = {
    'uploaded': {
        'pl': 'Plik został załadowany i przetwarzanie się rozpoczęło',
        'en': 'File uploaded and processing started',
    },
    'no_meta': {
        'pl': 'Plik z metadanymi nie został znaleziony',
        'en': 'Metadata file wasn\'t found',
    },
    'subject': {
        'pl': 'Wynik analizy',
        'en': 'Analysis result'
    },
    'invalid_meta': {
        'pl': 'Struktura pliku z metadanymi nie jest właściwa. Przetwarzanie zostało przerwane',
        'en': 'Inappropriate metadata file was found. Processing rejected',
    },
    'nlp_failed': {
        'pl': 'Zewnętrzna usługa nie jest dostępna. Spróbuj ponownie później',
        'en': 'External service unavailable. Please, retry later',
    },
    'updated_locs': {
        'pl': 'Dane zostały zmienione',
        'en': 'Locations were updated'
    },
    'failed_update_locs': {
        'pl': 'Dane nie zostały zmienione',
        'en': 'Failed to update locations'
    },
    'url': {
        'pl': 'Link do wyników analizy',
        'en': 'Link to analysis results'
    }
}
