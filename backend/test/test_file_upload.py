import io


def test_corpus_upload_get(client):
    response = client.get('/upload')
    assert response.status_code == 405
    assert response.get_json()['message'] == 'The method is not allowed for the requested URL.'


def test_corpus_upload_no_file(client):
    response = client.post('/upload')
    assert response.status_code == 400


def test_corpus_upload_non_zip(client):
    file_payload = {
        'file': (io.BytesIO(b'test file content'), 'text.txt')
    }
    response = client.post('/upload', data=file_payload)
    assert response.status_code == 400
    assert response.get_json()['message'] == {'file': 'File is not valid ZIP archive'}


def test_valid_corpus_upload_pl(client, zipfile_payload):
    payload = {
        'file': zipfile_payload,
        'email': 'test@mail.com',
        'lang': 'pl'
    }
    response = client.post('/upload', data=payload)
    assert response.status_code == 200


def test_valid_corpus_upload_en(client, zipfile_payload):
    payload = {
        'file': zipfile_payload,
        'email': 'test@mail.com',
        'lang': 'en'
    }
    response = client.post('/upload', data=payload)
    assert response.status_code == 200


def test_corpus_upload_no_lang(client, zipfile_payload):
    payload = {
        'file': zipfile_payload,
        'email': 'test@mail.com'
    }
    response = client.post('/upload', data=payload)
    assert response.status_code == 400


def test_corpus_upload_no_email(client, zipfile_payload):
    payload = {
        'file': zipfile_payload,
    }
    response = client.post('/upload', data=payload)
    assert response.status_code == 400
