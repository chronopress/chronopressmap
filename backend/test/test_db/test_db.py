from pytest import approx

import db
from db.processing.locations_aggregator import LocationsAggregator


def test_start_empty_database():
    assert db.Result.objects().count() == 0


def test_db_can_save_result(result):
    result.save()
    assert db.Result.objects().count() == 1


def test_retrieve_result_by_uuid(result):
    result_uuid = result.uuid
    result.save()
    found_result = db.Result.objects(uuid=result_uuid).first()
    assert found_result.corpus_name == result.corpus_name


def test_can_save_result_with_count(result, location):
    corpus_part_file = result.corpus_part_files[0]
    location.count = 42
    location.corpus_part_file = corpus_part_file
    result.locations.append(location)
    corpus_part_file.save()
    location.save()
    result.save()
    assert db.Result.objects().count() == 1

    saved_result = db.Result.objects(uuid=result.uuid).first()
    saved_location = db.Location.objects(uuid=location.uuid).first()
    assert saved_location == saved_result.locations[0]
    assert saved_location.count == 42
    saved_corpus_part_file = db.CorpusPartFile.objects(uuid=corpus_part_file.uuid).first()
    assert saved_corpus_part_file == saved_location.corpus_part_file
    assert saved_corpus_part_file.filename == corpus_part_file.filename


def test_can_save_corpus_with_locations(result):
    corpus_part_file = result.corpus_part_files[0]
    locations = [
        db.Location(
            name='loc_1',
            count=2,
            center_lng=1.0,
            center_lat=2.0,
            is_center_preferred=True,
            geometry=db.LineString(
                geo_json={'type': 'LineString', 'coordinates': [[0.0, 0.0], [2.0, 4.0]]}),
            corpus_part_file=corpus_part_file
        )
    ]
    corpus_part_file.save()
    result.locations = locations
    for location in locations:
        location.save()

    result.save()
    assert db.Result.objects().count() == 1

    saved_result = db.Result.objects().first()
    saved_locations = saved_result.locations
    assert saved_locations == locations
    assert saved_locations[0].name == locations[0].name
    assert db.CorpusPartFile.objects().count() == 1
    saved_corpus_part_file = db.CorpusPartFile.objects().first()
    assert saved_corpus_part_file == saved_result.corpus_part_files[0]
    assert saved_corpus_part_file.filename == corpus_part_file.filename
    assert db.Location.objects().count() == 1
    saved_location = db.Location.objects().first()
    assert saved_location == saved_locations[0]


def test_can_save_corpus_with_categories(result, dummy_categories):
    corpus_part_file = result.corpus_part_files[0]
    corpus_part_file.categories = dummy_categories
    corpus_part_file.save()
    result.save()
    assert db.Result.objects().count() == 1
    saved_result = db.Result.objects().first()
    saved_categories = saved_result.corpus_part_files[0].categories
    assert saved_categories == dummy_categories
    assert db.CorpusPartFile.objects().count() == 1
    saved_corpus_part_file = db.CorpusPartFile.objects().first()
    assert saved_corpus_part_file.categories == saved_result.corpus_part_files[0].categories


def test_aggregator_computes_proper_aggregations(locations):
    aggregator = LocationsAggregator(3)
    aggregations, non_aggregable = aggregator.aggregate(locations)
    assert len(aggregations) == 3
    assert len(non_aggregable) == 1
    expected_aggregations_level_0 = [
        db.Aggregation(locations=[locations[0], locations[1], locations[2], locations[3]],
                       geometry=db.Point(
                           geo_json={'type': 'Point', 'coordinates': [18.93243, 54.43856]})),
        db.Aggregation(locations=[locations[4]],
                       geometry=db.Point(
                           geo_json={'type': 'Point', 'coordinates': [-9.13930, 38.7223]}))
    ]
    expected_aggregations_level_1 = [
        db.Aggregation(locations=[locations[0], locations[1], locations[2]],
                       geometry=db.Point(
                           geo_json={'type': 'Point', 'coordinates': [17.47752, 50.87959]})),
        db.Aggregation(locations=[locations[3]],
                       geometry=db.Point(
                           geo_json={'type': 'Point', 'coordinates': [25.4651, 65.0121]})),
        db.Aggregation(locations=[locations[4]],
                       geometry=db.Point(
                           geo_json={'type': 'Point', 'coordinates': [-9.13930, 38.7223]}))
    ]
    expected_aggregations_level_2 = [
        db.Aggregation(locations=[locations[0]],
                       geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [17.0385, 51.1079]})),
        db.Aggregation(locations=[locations[1]],
                       geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [17.9231, 50.6683]})),
        db.Aggregation(locations=[locations[2]],
                       geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [17.46680, 50.86090]})),
        db.Aggregation(locations=[locations[3]],
                       geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [25.4651, 65.0121]})),
        db.Aggregation(locations=[locations[4]],
                       geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [-9.13930, 38.7223]}))
    ]
    expected_aggregation_levels = [
        db.AggregationLevel(level=0, aggregations=expected_aggregations_level_0),
        db.AggregationLevel(level=1, aggregations=expected_aggregations_level_1),
        db.AggregationLevel(level=2, aggregations=expected_aggregations_level_2)
    ]
    expected_non_aggregable = [locations[5]]
    for expected_aggr_level, aggregation_level in zip(expected_aggregation_levels, aggregations):
        assert expected_aggr_level.level == aggregation_level.level
        for expected_aggr, aggr in zip(expected_aggr_level.aggregations, aggregation_level.aggregations):
            assert expected_aggr.locations == aggr.locations
            expected_coordinates = expected_aggr.geometry.geo_json['coordinates']
            aggr_coordinates = aggr.geometry.geo_json['coordinates']
            assert aggr_coordinates == approx(expected_coordinates, rel=1e-4)

    assert expected_non_aggregable == non_aggregable


def test_can_save_corpus_with_aggregations(result, locations, corpus_part_file):
    aggregator = LocationsAggregator(3)
    aggregations_levels, non_aggregable = aggregator.aggregate(locations)
    result.locations = locations
    result.locations_aggregations = aggregations_levels
    result.non_aggregable_locations = non_aggregable

    corpus_part_file.save()
    for location in locations:
        location.save()
    result.save()

    assert db.Result.objects().count() == 1
    saved_result = db.Result.objects().first()
    assert saved_result.locations_aggregations == aggregations_levels
    assert saved_result.non_aggregable_locations == non_aggregable
    saved_location = db.Location.objects(uuid=aggregations_levels[0].aggregations[0].locations[0].uuid).first()
    assert saved_location == locations[0]
