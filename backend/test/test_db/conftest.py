import mongoengine
import pytest

from constants import MONGO_DB_ALIAS
import db


@pytest.fixture(autouse=True)
def db_setup():
    db_name = 'mongoenginetest'
    conn = mongoengine.connect(db=db_name,
                               host='mongomock://localhost',
                               alias=MONGO_DB_ALIAS)
    yield
    conn.drop_database(db_name)
    mongoengine.disconnect()


@pytest.fixture
def result(corpus_part_file, all_categories):
    result = db.Result(corpus_name='corpus_name',
                       corpus_part_files=[corpus_part_file],
                       all_categories=all_categories)
    return result


@pytest.fixture
def corpus_part_file(dummy_categories):
    return db.CorpusPartFile(filename='test', categories=dummy_categories)


@pytest.fixture
def location():
    return db.Location(
        name='city',
        count=1,
        center_lng=0.0,
        center_lat=0.0,
        is_center_preferred=True,
        geometry=db.Point(
            geo_json={'type': 'Point', 'coordinates': [0.0, 0.0]})
    )


@pytest.fixture
def all_categories():
    return {
        "journal_title": [
            "Trybuna Ludu", "Sztandar Młodych", "Słowo Polskie",
            "Gazeta Robotnicza", "Przegląd Sportowy", "Życie Warszawy",
            "Tygodnik Powszechny", "Przyjaciółka"
        ],
        "article_title": [
            "Odpowiedź Związku Radzieckiego na polskie memorandum pierwszym krokiem w ki",
            "Ja nie zapomniałam", "Droga do odzyskanej radości",
            "Sukces,  który zobowiązuje ", "Będziemy pościć trzy razy w tygodniu",
            "Środki zaradcze muszą się znaleźć",
            "Główny Komitet Kultury Fizycznej",
            "W naszym ręku jest oręż niezastąpiony",
            "Poważne sukcesy dolnośląskiej organizacji partyjnej",
            "Młodość świata defilowała na Łużnikach"
        ],
        "authors": [
            "Jadwiga S.", "Jerzy Bogusławski", "Klemens Krzyżagórski",
            "Tadeusz Tułasiewicz", "Oskar Stefański"
        ],
        "publication_date": [
            "1950-02-18", "1957-07-29", "1953-05-28", "1953-12-13", "1946-03-11",
            "1946-12-05", "1953-04-22", "1953-03-29", "1960-02-28", "1958-03-06"
        ],
        "exposition": [1, 2],
        "medium": ["Prasa drukowana", "paper"],
        "period": ["d", "tygodnik", "dziennik"],
        "status": ["Obieg oficjalny krajowy", "1_obieg"],
        "style": ["język prasy", "press"]
    }


@pytest.fixture
def dummy_categories():
    return {
        "journal_title": "Sztandar Młodych",
        "article_title": "Sukces,  który zobowiązuje ",
        "publication_date": "1953-05-28",
        "medium": "Prasa drukowana",
        "period": "dziennik",
        "status": "Obieg oficjalny krajowy",
        "style": "język prasy"
    }


@pytest.fixture()
def locations(corpus_part_file):
    locs = [
        db.Location(
            name='Wroclaw',
            count=2,
            center_lng=17.0385,
            center_lat=51.1079,
            is_center_preferred=True,
            geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [17.0385, 51.1079]}),
            corpus_part_file=corpus_part_file
        ),
        db.Location(
            name='Opole',
            count=1,
            center_lng=17.9231,
            center_lat=50.6683,
            is_center_preferred=True,
            geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [17.9231, 50.6683]}),
            corpus_part_file=corpus_part_file
        ),
        db.Location(
            name='Brzeg',
            count=3,
            center_lng=17.4668,
            center_lat=50.8609,
            is_center_preferred=True,
            geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [17.4668, 50.8609]}),
            corpus_part_file=corpus_part_file
        ),
        db.Location(
            name='Oulu',
            count=1,
            center_lng=25.4651,
            center_lat=65.0121,
            is_center_preferred=True,
            geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [25.4651, 65.0121]}),
            corpus_part_file=corpus_part_file
        ),
        db.Location(
            name='Lizbona',
            count=1,
            center_lng=-9.1393,
            center_lat=38.7223,
            is_center_preferred=True,
            geometry=db.Point(geo_json={'type': 'Point', 'coordinates': [-9.1393, 38.7223]}),
            corpus_part_file=corpus_part_file
        ),
        db.Location(
            name='Andora',
            count=1,
            center_lng=1.5732033,
            center_lat=42.5407167,
            is_center_preferred=False,
            geometry=db.Polygon(
                geo_json={'type': 'Polygon', 'coordinates': [[[1.4135781, 42.5353776],
                                                              [1.5157512, 42.4288238],
                                                              [1.786664, 42.5741828],
                                                              [1.4934425, 42.6535572],
                                                              [1.4135781, 42.5353776]]]}
            ),
            corpus_part_file=corpus_part_file
        )
    ]
    return locs
