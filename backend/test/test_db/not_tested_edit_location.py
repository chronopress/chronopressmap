import db
import json


def test_can_update_locations(result, client):
    corpus_part_file = result.corpus_part_files[0]
    location = db.Location(
        name="loc_1",
        count=2,
        center_lng=1.0,
        center_lat=2.0,
        is_center_preferred=True,
        geometry=db.Point(geo_json={"type": "Point", "coordinates": [1.0, 2.0]}),
        corpus_part_file=corpus_part_file,
    )
    uuid = location.uuid
    location.save()

    new_location = [12.45, 13.54]
    payload = {
        "locations": [
            {
                "uuid": uuid,
                "center_lng": new_location[0],
                "center_lat": new_location[1],
            }
        ],
        "lang": "pl",
    }
    response = client.post(
        "/edit_locations", data=json.dumps(payload), content_type="application/json"
    )
    assert response.status_code == 200
