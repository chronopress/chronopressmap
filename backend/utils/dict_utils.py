
def omit_empty(dict_):
    return {key: value for key, value in dict_.items() if value}
