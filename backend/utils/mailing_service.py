import smtplib
from email.mime.text import MIMEText

from constants import MAIL_CREDENTIALS, WEBSITE_URL
from lang.responses import LANG_RESPONSES

SMTP_SERVER_DATA = {
    'address': 'smtp.gmail.com',
    'port': 465
}


class MailingException(Exception):
    pass


def construct_mail(subject, body, msg_type):
    mail = MIMEText(body, msg_type)
    mail['Subject'] = subject

    return mail


def send_mail(receiver_address, subject, body, msg_type='plain'):
    mail = construct_mail(subject, body, msg_type)

    with smtplib.SMTP_SSL(SMTP_SERVER_DATA['address'], SMTP_SERVER_DATA['port']) as server:
        server.login(MAIL_CREDENTIALS['address'], MAIL_CREDENTIALS['password'])
        server.sendmail(MAIL_CREDENTIALS['address'], receiver_address, mail.as_string())


def send_analysis_result(uuid, mail_address, lang):
    try:
        send_mail(
            receiver_address=mail_address,
            subject=LANG_RESPONSES['subject'][lang],
            body=f'<a href="{WEBSITE_URL}/result/{uuid}">{LANG_RESPONSES["url"][lang]}</a>',
            msg_type='html'
        )
    except Exception as e:
        raise MailingException('Sending mail failed with msg: ' + str(e))


def send_failed_analysis_result(mail_address, lang, msg_type, filename):
    try:
        send_mail(
            receiver_address=mail_address,
            subject=LANG_RESPONSES['subject'][lang],
            body=f'{filename} - {LANG_RESPONSES[msg_type][lang]}'
        )
    except Exception as e:
        raise MailingException('Sending mail failed with msg: ' + str(e))
