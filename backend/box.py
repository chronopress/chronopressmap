from box_impl import BoxImpl
from utils.singleton import Singleton


class Box(BoxImpl, metaclass=Singleton):
    pass


def init_box(cache_handler):
    Box(cache_handler)
