import json

from db.processing.locations_aggregator import LocationsAggregator
from geo_location.geo_code.geo_code_provider_interface import GeoCodeProviderInterface
from geo_location.geo_code.geo_code_access_factory import GeoCodeAccessFactory
from geo_location.geo_code.geo_code_provider import GeoCodeProvider
from geo_location.geo_code.geo_code_provider_cache_decorator import GeoCodeProviderCacheDecorator

from nlp.lemma.lemmatizer_wrapper import LemmatizerWrapper

from nlp.ner.ner_access import NerAccess

from constants import MAIN_CONFIGURATION_FILE

from logger import logger


class BoxImpl(object):
    def __init__(self, cache_handler=None):
        self.app_config: json
        self.geo_code_provider: GeoCodeProviderInterface
        self.ner_access: NerAccess
        self.lemmatizer_wrapper: LemmatizerWrapper
        self.locations_aggregator: LocationsAggregator
        self._init_app_commons(cache_handler)

    def _init_app_commons(self, cache_handler):
        self.app_config = self._read_app_config()
        self.geo_code_provider = self._init_geo_code_provider(self.app_config, cache_handler)
        self.ner_access = self._init_ner_access(self.app_config)
        self.lemmatizer_wrapper = self._init_lemmatizer_wrapper()
        self.locations_aggregator = self._init_locations_aggregator(self.app_config)

    def _read_app_config(self):
        with open(MAIN_CONFIGURATION_FILE, "r") as f:
            return json.load(f)

    def _init_geo_code_provider(self, app_config, cache_handler):
        logger.debug('Initializing geocode provider.')
        geo_access_factory = GeoCodeAccessFactory()
        geo_code_access = geo_access_factory.make(app_config)
        logger.debug('Geocode provider initialized')
        return GeoCodeProviderCacheDecorator(GeoCodeProvider(geo_code_access), cache_handler)

    def _init_ner_access(self, app_config):
        return NerAccess(app_config['ner']['model'])

    def _init_lemmatizer_wrapper(self):
        return LemmatizerWrapper()

    def _init_locations_aggregator(self, app_config):
        return LocationsAggregator(app_config['aggregations']['levelsNum'])
