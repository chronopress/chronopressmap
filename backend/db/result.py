import datetime

from mongoengine import (
    DateTimeField,
    Document,
    EmbeddedDocumentListField,
    StringField,
    DictField,
    ReferenceField,
    ListField,
    PULL,
    DENY
)


from constants import MONGO_DB_ALIAS
from utils.common import get_uuid
from .corpus_part_file import CorpusPartFile
from .aggregation import AggregationLevel
from .location import Location


class Result(Document):
    uuid = StringField(default=get_uuid, primary_key=True)
    date = DateTimeField(default=datetime.datetime.now)
    corpus_name = StringField(required=True)
    corpus_part_files = ListField(ReferenceField(CorpusPartFile, reverse_delete_rule=DENY))
    all_categories = DictField(required=True)
    locations_aggregations = EmbeddedDocumentListField(AggregationLevel)
    non_aggregable_locations = ListField(ReferenceField(Location, reverse_delete_rule=PULL))
    locations = ListField(ReferenceField(Location, reverse_delete_rule=PULL))

    meta = {
        'db_alias': MONGO_DB_ALIAS,
        'collection': 'results'
    }
