from mongoengine import (
    EmbeddedDocument,
    PolygonField,
    PointField,
    MultiPointField,
    LineStringField,
    MultiLineStringField,
    MultiPolygonField
)


class Geometry(EmbeddedDocument):
    meta = {
        'allow_inheritance': True,
        'abstract': True
    }


class Point(Geometry):
    geo_json = PointField(required=True)


class MultiPoint(Geometry):
    geo_json = MultiPointField(required=True)


class LineString(Geometry):
    geo_json = LineStringField(required=True)


class MultiLineString(Geometry):
    geo_json = MultiLineStringField(required=True)


class Polygon(Geometry):
    geo_json = PolygonField(required=True)


class MultiPolygon(Geometry):
    geo_json = MultiPolygonField(required=True)


def make_geometry(geo_json):
    geometry_type = geo_json['type']
    if geometry_type == 'Point':
        return Point(geo_json=geo_json)
    elif geometry_type == 'MultiPoint':
        return MultiPoint(geo_json=geo_json)
    elif geometry_type == 'LineString':
        return LineString(geo_json=geo_json)
    elif geometry_type == 'MultiLineString':
        return MultiLineString(geo_json=geo_json)
    elif geometry_type == 'Polygon':
        return Polygon(geo_json=geo_json)
    elif geometry_type == 'MultiPolygon':
        return MultiPolygon(geo_json=geo_json)
    else:
        raise ValueError(f'Wrong geo_json provided, invalid type: {geometry_type}')
