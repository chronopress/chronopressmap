from dataclasses import dataclass
from typing import Iterable, Dict, Any

from mongoengine import (
    EmbeddedDocument,
    IntField,
    ListField,
    ReferenceField,
    EmbeddedDocumentField,
    EmbeddedDocumentListField
)

from .geometry import Point
from .location import Location


@dataclass
class AggregationData:
    locations: Iterable[Location]
    center_point_geo_json: Dict[str, Any]


class Aggregation(EmbeddedDocument):
    locations = ListField(ReferenceField(Location))
    geometry = EmbeddedDocumentField(Point)

    @staticmethod
    def from_aggregation_data(aggregation_data: AggregationData):
        return Aggregation(locations=aggregation_data.locations,
                           geometry=Point(geo_json=aggregation_data.center_point_geo_json))


class AggregationLevel(EmbeddedDocument):
    level = IntField(required=True)
    aggregations = EmbeddedDocumentListField(Aggregation)

    @staticmethod
    def from_locations_clusters(level, locations_clusters: Iterable[AggregationData]):
        aggregations = [Aggregation.from_aggregation_data(aggregation_data)
                        for aggregation_data in locations_clusters]
        return AggregationLevel(level=level, aggregations=aggregations)
