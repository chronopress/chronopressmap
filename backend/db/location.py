from mongoengine import (
    StringField,
    IntField,
    FloatField,
    BooleanField,
    EmbeddedDocumentField,
    Document,
    ReferenceField,
    CASCADE
)

from constants import MONGO_DB_ALIAS
from utils.common import get_uuid
from .corpus_part_file import CorpusPartFile
from .geometry import Geometry, make_geometry
from geo_location.geo_code.geo_code_provider_interface import GeoCodeProviderInterface


class Location(Document):
    uuid = StringField(default=get_uuid, primary_key=True)
    name = StringField(required=True)
    count = IntField(default=1)
    center_lng = FloatField(required=True)
    center_lat = FloatField(required=True)
    is_center_preferred = BooleanField(required=True)
    geometry = EmbeddedDocumentField(Geometry)
    corpus_part_file = ReferenceField(CorpusPartFile, reverse_delete_rule=CASCADE)

    meta = {
        'db_alias': MONGO_DB_ALIAS,
        'collection': 'locations'
    }

    @staticmethod
    def from_geocode(geo_code: GeoCodeProviderInterface.GeoCode, corpus_part_file: CorpusPartFile):
        return Location(
            name=geo_code.name,
            count=geo_code.count,
            center_lng=geo_code.geo_code_data.center_lng,
            center_lat=geo_code.geo_code_data.center_lat,
            is_center_preferred=geo_code.geo_code_data.is_center_preferred,
            geometry=make_geometry(geo_code.geo_code_data.geo_json),
            corpus_part_file=corpus_part_file
        )
