from mongoengine import (
    StringField,
    DictField,
    Document
)

from constants import MONGO_DB_ALIAS
from utils.common import get_uuid


class CorpusPartFile(Document):
    uuid = StringField(default=get_uuid, primary_key=True)
    filename = StringField(required=True)
    categories = DictField(required=True)

    meta = {
        'db_alias': MONGO_DB_ALIAS,
        'collection': 'corpus_part_files'
    }
