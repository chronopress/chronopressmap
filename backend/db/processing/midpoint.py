from typing import Iterable

import numpy as np

from db import Location


class Midpoint(object):
    """ See geomidpoint.com/calculation.html as reference """

    @staticmethod
    def compute_midpoint(locations: Iterable[Location]):
        Lng, Lat = Midpoint._convert_coordinates_to_radians(locations)
        X, Y, Z = Midpoint._to_cartesian_coordinates(Lng, Lat)
        x_mean, y_mean, z_mean = Midpoint._coordinates_mean(X, Y, Z)
        lng_center, lat_center = Midpoint._to_lng_lat_coordinates(x_mean, y_mean, z_mean)
        return np.degrees(lng_center), np.degrees(lat_center)

    @staticmethod
    def _convert_coordinates_to_radians(locations):
        return np.hsplit(np.radians([[location.center_lng, location.center_lat]
                                     for location in locations]), 2)

    @classmethod
    def _to_cartesian_coordinates(cls, Lng, Lat):
        X = np.cos(Lat) * np.cos(Lng)
        Y = np.cos(Lat) * np.sin(Lng)
        Z = np.sin(Lat)
        return X, Y, Z

    @classmethod
    def _coordinates_mean(cls, X, Y, Z):
        return np.mean(X), np.mean(Y), np.mean(Z)

    @classmethod
    def _to_lng_lat_coordinates(cls, x_mean, y_mean, z_mean):
        return np.arctan2(y_mean, x_mean), np.arctan2(z_mean, np.sqrt(x_mean * x_mean + y_mean * y_mean))
