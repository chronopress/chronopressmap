from typing import List

import pygeohash

from db import Location


class GeohashClusteringException(Exception):
    pass


class GeohashClusteringAlgo(object):
    GEOHASH_PREFIX_MAX_LEN = 12
    LEVELS_MAX_NUM = 23

    QUARTERS_ODD_PREFIX_LEN = {
        'p': 0, 'r': 0, 'n': 0, 'q': 0, 'j': 0, 'm': 0, 'h': 0, 'k': 0,
        'x': 1, 'z': 1, 'w': 1, 'y': 1, 't': 1, 'v': 1, 's': 1, 'u': 1,
        '5': 2, '7': 2, '4': 2, '6': 2, '1': 2, '3': 2, '0': 2, '2': 2,
        'e': 3, 'g': 3, 'd': 3, 'f': 3, '9': 3, 'c': 3, '8': 3, 'b': 3
    }

    QUARTERS_EVEN_PREFIX_LEN = {
        'b': 0, 'c': 0, 'f': 0, 'g': 0, '8': 0, '9': 0, 'd': 0, 'e': 0,
        'u': 1, 'v': 1, 'y': 1, 'z': 1, 's': 1, 't': 1, 'w': 1, 'x': 1,
        '2': 2, '3': 2, '6': 2, '7': 2, '0': 2, '1': 2, '4': 2, '5': 2,
        'k': 3, 'm': 3, 'q': 3, 'r': 3, 'h': 3, 'j': 3, 'n': 3, 'p': 3
    }

    def __init__(self, levels_num, locations: List[Location]):
        self._levels_num = levels_num
        self._locations = locations
        self._geohashes = GeohashClusteringAlgo._make_geo_hashes(locations)

    def clustering(self, level):
        geohash_prefix_len = self._get_geohash_prefix_len(level)
        return self.make_locations_clusters(geohash_prefix_len, level)

    def _get_geohash_prefix_len(self, level):
        if level == self._levels_num - 1:
            return GeohashClusteringAlgo.GEOHASH_PREFIX_MAX_LEN
        elif level < self._levels_num - 1:
            return level // 2 + 1 if level % 2 == 0 else level // 2 + 2
        else:
            raise GeohashClusteringException('Zoom level is higher than declared maximum level num')

    def make_locations_clusters(self, geohash_prefix_len, level):
        locations_clusters = {}
        for location_index, geohash in enumerate(self._geohashes):
            geohash_prefix = GeohashClusteringAlgo._get_geohash_prefix(geohash, geohash_prefix_len)
            geohash_group = self._get_geohash_group(geohash_prefix, level)
            if geohash_group not in locations_clusters:
                locations_clusters[geohash_group] = []

            locations_clusters[geohash_group].append(self._locations[location_index])

        return locations_clusters.values()

    @staticmethod
    def _make_geo_hashes(locations):
        return [pygeohash.encode(longitude=location.center_lng, latitude=location.center_lat)
                for location in locations]

    @classmethod
    def _get_geohash_prefix(cls, geohash, geohash_prefix_len):
        return geohash[:geohash_prefix_len]

    def _get_geohash_group(self, geohash_prefix, level):
        if self._should_use_integral_prefix_group(level):
            return GeohashClusteringAlgo._get_integral_prefix_group(geohash_prefix)
        else:
            return GeohashClusteringAlgo._get_group_based_on_prefix_and_quarter(geohash_prefix)

    def _should_use_integral_prefix_group(self, level):
        return level % 2 == 0 or level == self._levels_num - 1

    @staticmethod
    def _get_integral_prefix_group(geohash_prefix):
        return geohash_prefix

    @staticmethod
    def _get_group_based_on_prefix_and_quarter(geohash_prefix):
        parent_cell_prefix = geohash_prefix[:-1]
        quarters = GeohashClusteringAlgo.QUARTERS_EVEN_PREFIX_LEN if len(parent_cell_prefix) % 2 == 0\
            else GeohashClusteringAlgo.QUARTERS_ODD_PREFIX_LEN

        prefix_last_char = geohash_prefix[-1]
        quarter_num = quarters[prefix_last_char]

        return parent_cell_prefix, quarter_num
