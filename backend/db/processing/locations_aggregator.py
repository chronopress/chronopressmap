from typing import List

from db import Location
from db.aggregation import AggregationLevel, AggregationData
from db.processing.geohash_clustering_algo import GeohashClusteringAlgo
from db.processing.midpoint import Midpoint


class LocationsAggregator(object):
    def __init__(self, levels_num):
        self._levels_num = levels_num

    def aggregate(self, locations: List[Location]) -> (List[AggregationLevel], List[Location]):
        aggregable, non_aggregable = LocationsAggregator._filter_aggregable_locations(locations)
        clustering_algo = GeohashClusteringAlgo(self._levels_num, aggregable)
        return self._make_aggregations(clustering_algo), non_aggregable

    @staticmethod
    def _filter_aggregable_locations(locations):
        aggregable = []
        non_aggregable = []
        for location in locations:
            aggregable.append(location) if location.is_center_preferred else non_aggregable.append(location)

        return aggregable, non_aggregable

    def _make_aggregations(self, clustering_algo):
        aggregations = []
        for level in range(0, self._levels_num):
            locations_clusters = [
                AggregationData(locations, LocationsAggregator._make_point_geojson(locations))
                for locations in clustering_algo.clustering(level)
            ]
            aggregations.append(AggregationLevel.from_locations_clusters(level, locations_clusters))

        return aggregations

    @staticmethod
    def _make_point_geojson(locations):
        lng_mid, lat_mid = Midpoint.compute_midpoint(locations)
        return {'type': 'Point', 'coordinates': [lng_mid, lat_mid]}
