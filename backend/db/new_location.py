from dataclasses import dataclass


@dataclass
class NewLocation:
    uuid: str
    center_lat: float
    center_lng: float
