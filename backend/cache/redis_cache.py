import json

import redis

from constants import REDIS_KEY_PREFIX


class RedisCache:
    def __init__(self, prefix=None, host=None, port=None, url=None, keysep=';', serializer=json):
        self.host = host
        self.port = port
        self.prefix = prefix if prefix else REDIS_KEY_PREFIX
        self.keysep = keysep
        self.serializer = serializer
        if url:
            self.redis = redis.from_url(url)
        else:
            self.redis = redis.Redis(host=host, port=port)

    def _get_key(self, key):
        return key.split(self.keysep)[1]

    def _make_key(self, key):
        return f'{self.prefix}{self.keysep}{key}'

    def set(self, key, value):
        set_key = self._make_key(key)
        value = self.serializer.dumps(value)
        self.redis.set(set_key, value)

    def get(self, key):
        set_key = self._make_key(key)
        value = self.redis.get(set_key)
        return self.serializer.loads(value) if value else None
